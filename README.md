# Coding examples

Here are fragments of my work, I'm able to show whole working code but only as presentation, not as public repository.

## Rest server
- example of my rest server there is shown part of OrdersController and nested used parts
- for resoures is used HATEOAS, for configuration is used Spring Cloud Configuration Server, for authentication is used OAuth   
- there is plan to convert it to microservice architecture orchestrated via Kafka and using CQRS with Event Sourcing (this is postponed for a while)
- most interesting part (for me :smirk: ) is in **rest-server/src/main/java/restserver/domain/workflow**
  - this is my generic tool for process internal state workflow of selected entity (here for example for order)
- in this project I created CI/CD from scratch (probably can be solved better, but it was my first time :sweat_smile: )
  - hook on GitLab
  - Jenkins processing (added **Jenkinsfile** in this coding example)
    - build and verify or build and deploy to my personal nexus (setup by maven settings.xml)
    - upload to application server and restart service via sshagent (secured connection via by public ssh key authentication)
    - notify GitLab with pipeline result

## Chart visualiser
- here I tried to visualise stock data and aggregate big data sets to be renderable
- I experimented with ElasticSearch and histogram aggregations (for candle creation from ticks), but sometimes I need to load many ticks and 
MySql with materialized views worked better for me 
- for selecting which indicator calculator class will be used I'm using strategy design pattern **chart-visualiser/src/main/java/chartvisualiser/services/ChartControllerService**
- data import using multi-thread file processing **chart-visualiser/src/main/java/chartvisualiser/domain/services/imports/ImportService**
- data fetching is implemented manually for few reasons (**chart-visualiser/src/main/java/chartvisualiser/domain/repositories/CandleRepository**)
  - because of performance - wanted to remove mapping from resultset to list of entities and then to list of objects
  - need to decide if i want to load some extra candles because of indicators' points aggregation
  - data is loaded from materialized views which are dynamically selected based on interval size and asset type
- in this example is my FE work, I'm using jQuery, Bootstrap and external library HighCharts.



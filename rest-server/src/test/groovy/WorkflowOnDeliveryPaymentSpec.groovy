package com.codeexamples.restserver.domain.workflow.order

import com.codeexamples.restserver.domain.entity.Order
import com.codeexamples.restserver.domain.entity.OrderItem
import com.codeexamples.restserver.domain.entity.Payment
import com.codeexamples.restserver.domain.workflow.order.events.*
import com.codeexamples.restserver.domain.workflow.order.listeners.OrderWorkflowEventListener
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.ApplicationEventPublisher
import spock.lang.Specification

@SpringBootTest
class WorkflowOnDeliveryPaymentSpec extends Specification {

    @Autowired
    private ApplicationEventPublisher eventPublisher

    Order order = new Order(items: [new OrderItem(type: OrderItem.ItemType.PAYMENT, payment: new Payment(type: Payment.PaymentType.ON_DELIVERY))])

    def "Test on delivery payment workflow"() {
        when:
            eventPublisher.publishEvent(new CreatedEvent(order))
            eventPublisher.publishEvent(new StartProcessingEvent(order))
            eventPublisher.publishEvent(new SentEvent(order))
            eventPublisher.publishEvent(new CompletedEvent(order))
            eventPublisher.publishEvent(new FinishedEvent(order))
        then:
            order.state == Order.StateEnum.DONE
    }

    def "Test on delivery payment workflow from processing state"() {
        given:
            order.state = Order.StateEnum.PROCESSING
        when:
            eventPublisher.publishEvent(new SentEvent(order))
            eventPublisher.publishEvent(new CompletedEvent(order))
            eventPublisher.publishEvent(new FinishedEvent(order))
        then:
            order.state == Order.StateEnum.DONE
    }

    def "Test on delivery payment workflow from processing state to waiting for payment on delivery"() {
        given:
            order.state = Order.StateEnum.PROCESSING
        when:
            eventPublisher.publishEvent(new SentEvent(order))
        then:
            order.state == Order.StateEnum.WAITING_FOR_ON_DELIVERY_PAYMENT
    }

    def "Test on delivery payment workflow rejected complaint"() {
        when:
            eventPublisher.publishEvent(new CreatedEvent(order))
            eventPublisher.publishEvent(new StartProcessingEvent(order))
            eventPublisher.publishEvent(new SentEvent(order))
            eventPublisher.publishEvent(new CompletedEvent(order))
            eventPublisher.publishEvent(new CreateComplaintEvent(order))
            eventPublisher.publishEvent(new FinishedEvent(order))
        then:
            order.state == Order.StateEnum.DONE
    }

    def "Test on delivery payment workflow success complaint"() {
        when:
            eventPublisher.publishEvent(new CreatedEvent(order))
            eventPublisher.publishEvent(new StartProcessingEvent(order))
            eventPublisher.publishEvent(new SentEvent(order))
            eventPublisher.publishEvent(new CompletedEvent(order))
            eventPublisher.publishEvent(new CreateComplaintEvent(order))
            eventPublisher.publishEvent(new RefundedEvent(order))
        then:
            order.state == Order.StateEnum.REFUNDED
    }

    def "Test on delivery payment workflow returned in 14 days"() {
        when:
            eventPublisher.publishEvent(new CreatedEvent(order))
            eventPublisher.publishEvent(new StartProcessingEvent(order))
            eventPublisher.publishEvent(new SentEvent(order))
            eventPublisher.publishEvent(new CompletedEvent(order))
            eventPublisher.publishEvent(new ReturnedEvent(order))
            eventPublisher.publishEvent(new RefundedEvent(order))
        then:
            order.state == Order.StateEnum.REFUNDED
    }

    def "Test on delivery payment workflow not taken over"() {
        when:
            eventPublisher.publishEvent(new CreatedEvent(order))
            eventPublisher.publishEvent(new StartProcessingEvent(order))
            eventPublisher.publishEvent(new SentEvent(order))
            eventPublisher.publishEvent(new NotTakenOverEvent(order))
            eventPublisher.publishEvent(new ReturnedEvent(order))
        then:
        order.state == Order.StateEnum.DONE
    }

    def "Test on delivery payment workflow cancelled from processing"() {
        when:
            eventPublisher.publishEvent(new CreatedEvent(order))
            eventPublisher.publishEvent(new StartProcessingEvent(order))
            eventPublisher.publishEvent(new CancelledEvent(order))
        then:
            order.state == Order.StateEnum.DONE
    }
}

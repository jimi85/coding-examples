package com.codeexamples.restserver.domain.workflow.order

import com.codeexamples.restserver.domain.entity.Order
import com.codeexamples.restserver.domain.entity.OrderItem
import com.codeexamples.restserver.domain.entity.Payment
import com.codeexamples.restserver.domain.workflow.order.events.*
import com.codeexamples.restserver.domain.workflow.order.listeners.OrderWorkflowEventListener
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.ApplicationEventPublisher
import spock.lang.Specification

@SpringBootTest
class WorkflowOnlinePaymentSpec extends Specification {

    @Autowired
    ApplicationEventPublisher eventPublisher

    Order order = new Order(items: [new OrderItem(type: OrderItem.ItemType.PAYMENT, payment: new Payment(type: Payment.PaymentType.GO_PAY))])

    def "Test on-line payment workflow"() {
        when:
            eventPublisher.publishEvent(new CreatedEvent(order))
            eventPublisher.publishEvent(new OnlinePaymentSuccessEvent(order))
            eventPublisher.publishEvent(new StartProcessingEvent(order))
            eventPublisher.publishEvent(new SentEvent(order))
            eventPublisher.publishEvent(new CompletedEvent(order))
            eventPublisher.publishEvent(new FinishedEvent(order))
        then:
            order.state == Order.StateEnum.DONE
    }

    def "Test on-line payment workflow from sent state"() {
        given:
            order.state = Order.StateEnum.SENT
        when:
            eventPublisher.publishEvent(new CompletedEvent(order))
            eventPublisher.publishEvent(new FinishedEvent(order))
        then:
            order.state == Order.StateEnum.DONE
    }

    def "Test on-line payment workflow rejected complaint"() {
        when:
            eventPublisher.publishEvent(new CreatedEvent(order))
            eventPublisher.publishEvent(new OnlinePaymentSuccessEvent(order))
            eventPublisher.publishEvent(new StartProcessingEvent(order))
            eventPublisher.publishEvent(new SentEvent(order))
            eventPublisher.publishEvent(new CompletedEvent(order))
            eventPublisher.publishEvent(new CreateComplaintEvent(order))
            eventPublisher.publishEvent(new FinishedEvent(order))
        then:
            order.state == Order.StateEnum.DONE
    }

    def "Test on-line payment workflow success complaint"() {
        when:
            eventPublisher.publishEvent(new CreatedEvent(order))
            eventPublisher.publishEvent(new OnlinePaymentSuccessEvent(order))
            eventPublisher.publishEvent(new StartProcessingEvent(order))
            eventPublisher.publishEvent(new SentEvent(order))
            eventPublisher.publishEvent(new CompletedEvent(order))
            eventPublisher.publishEvent(new CreateComplaintEvent(order))
            eventPublisher.publishEvent(new RefundedEvent(order))
        then:
            order.state == Order.StateEnum.REFUNDED
    }

    def "Test on-line payment workflow returned in 14 days"() {
        when:
            eventPublisher.publishEvent(new CreatedEvent(order))
            eventPublisher.publishEvent(new OnlinePaymentSuccessEvent(order))
            eventPublisher.publishEvent(new StartProcessingEvent(order))
            eventPublisher.publishEvent(new SentEvent(order))
            eventPublisher.publishEvent(new CompletedEvent(order))
            eventPublisher.publishEvent(new ReturnedEvent(order))
            eventPublisher.publishEvent(new RefundedEvent(order))
        then:
            order.state == Order.StateEnum.REFUNDED
    }

    def "Test on-line payment workflow not taken over"() {
        given:
            order.paid = true
        when:
            eventPublisher.publishEvent(new CreatedEvent(order))
            eventPublisher.publishEvent(new OnlinePaymentSuccessEvent(order))
            eventPublisher.publishEvent(new StartProcessingEvent(order))
            eventPublisher.publishEvent(new SentEvent(order))
            eventPublisher.publishEvent(new NotTakenOverEvent(order))
            eventPublisher.publishEvent(new ReturnedEvent(order))
            eventPublisher.publishEvent(new RefundedEvent(order))
        then:
            order.state == Order.StateEnum.REFUNDED
    }

    def "Test on-line payment workflow cancelled from waiting for payment"() {
        when:
            eventPublisher.publishEvent(new CreatedEvent(order))
            eventPublisher.publishEvent(new CancelledEvent(order))
        then:
            order.state == Order.StateEnum.DONE
    }

    def "Test on-line payment workflow cancelled from payment failed"() {
        when:
            eventPublisher.publishEvent(new CreatedEvent(order))
            eventPublisher.publishEvent(new OnlinePaymentFailedEvent(order))
            eventPublisher.publishEvent(new OnlinePaymentFailedEvent(order))
            eventPublisher.publishEvent(new OnlinePaymentFailedEvent(order))
            eventPublisher.publishEvent(new CancelledEvent(order))
        then:
            order.state == Order.StateEnum.DONE
    }

    def "Test on-line payment workflow cancelled from processing"() {
        given:
            order.paid = true
        when:
            eventPublisher.publishEvent(new CreatedEvent(order))
            eventPublisher.publishEvent(new OnlinePaymentSuccessEvent(order))
            eventPublisher.publishEvent(new StartProcessingEvent(order))
            eventPublisher.publishEvent(new CancelledEvent(order))
            eventPublisher.publishEvent(new RefundedEvent(order))
        then:
            order.state == Order.StateEnum.REFUNDED
    }
}

package com.codeexamples.restserver.services;

import com.codeexamples.restserver.controllers.AdminOrdersController;
import com.codeexamples.restserver.controllers.mappers.OrderMapper;
import com.codeexamples.restserver.controllers.requests.OrderItemPostRequest;
import com.codeexamples.restserver.controllers.requests.OrderItemPutRequest;
import com.codeexamples.restserver.controllers.requests.OrderPostRequest;
import com.codeexamples.restserver.controllers.requests.OrderPutRequest;
import com.codeexamples.restserver.controllers.resources.OrderAdminResource;
import com.codeexamples.restserver.controllers.resources.OrderPublicResource;
import com.codeexamples.restserver.domain.dto.OrderAction;
import com.codeexamples.restserver.domain.dto.OrderDto;
import com.codeexamples.restserver.domain.service.OrderService;
import com.codeexamples.restserver.libraries.onlinepayment.GoPayService;
import com.codeexamples.restserver.libraries.onlinepayment.domain.exception.NotFoundException;
import com.codeexamples.restserver.libraries.onlinepayment.dto.CreateGoPayPaymentDto;
import com.codeexamples.restserver.libraries.onlinepayment.dto.Currency;
import com.codeexamples.restserver.libraries.onlinepayment.dto.GoPayPaymentDto;
import com.codeexamples.restserver.utils.HateoasPagingUtils;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.data.domain.Page;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Service
@RequiredArgsConstructor
public class OrdersControllerService {

    private final OrderService orderService;
    private final OrderMapper orderMapper;
    private final GoPayService goPayService;

    public EntityModel<OrderPublicResource> createOrder(OrderPostRequest request, String eshopId) {
        OrderDto order = orderService.createOrder(orderMapper.toPostDto(request, eshopId));
        if (order.isOnlinePaid()) {
            GoPayPaymentDto payment = createGoPayPayment(order);
            return EntityModel.of(orderMapper.toPublicResource(order, payment));
        }
        return EntityModel.of(orderMapper.toPublicResource(order));
    }

    public EntityModel<OrderPublicResource> getOrder(String orderId, String eshopId) {
        OrderDto order = orderService.getOrder(UUID.fromString(orderId), UUID.fromString(eshopId));
        if (order.isOnlinePaid()) {
            GoPayPaymentDto payment;
            try {
                payment = goPayService.getGoPayPayment(order.getExtId(), order.getEshopExtId());
            } catch (NotFoundException e) {
                payment = createGoPayPayment(order);
            }

            if (payment.isTimeouted()) {
                payment = createGoPayPayment(order);
            }
            return EntityModel.of(orderMapper.toPublicResource(order, payment));
        }
        return EntityModel.of(orderMapper.toPublicResource(order));
    }

    private GoPayPaymentDto createGoPayPayment(OrderDto order) {
        return goPayService.createPayment(
                orderMapper.toCreateGoPayDto(
                        order, CreateGoPayPaymentDto.Payer.PaymentMethod.PAYMENT_CARD, Currency.CZK, CreateGoPayPaymentDto.Lang.CS
                )
        );
    }

    public PagedModel<OrderAdminResource> getAdminOrders(String search, Integer page, String sort, String eshopId) {
        Page<OrderDto> orders = orderService.getOrders(search, page, sort, UUID.fromString(eshopId));

        return PagedModel.of(
                orderMapper.toAdminResources(orders),
                HateoasPagingUtils.getPageMetadata(orders),
                linkTo(methodOn(AdminOrdersController.class).getOrders(eshopId, search, page, sort)).withSelfRel()
        );
    }
}

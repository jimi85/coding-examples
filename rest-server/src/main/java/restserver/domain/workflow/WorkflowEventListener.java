package com.codeexamples.restserver.domain.workflow;

public interface WorkflowEventListener<E extends WorkflowEntity<? extends Enum<?>>> {
     void handleWorkflowEvent(Event<E> event);
}

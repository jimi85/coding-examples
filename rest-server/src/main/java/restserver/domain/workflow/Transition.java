package com.codeexamples.restserver.domain.workflow;

import lombok.EqualsAndHashCode;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@EqualsAndHashCode(onlyExplicitlyIncluded = true, doNotUseGetters = true)
public class Transition<E extends WorkflowEntity<S>, S extends Enum<S>> {

    @EqualsAndHashCode.Include
    private final State<E, S> nextState;
    private final Set<Condition<?>> conditions = new HashSet<>();

    public Transition(State<E, S> nextState, Set<Condition<?>> conditions) {
        this.nextState = nextState;
        this.conditions.addAll(conditions);
    }

    public Optional<State<E, S>> getNextState() {
        if (isConditionsSatisfied()) {
            return Optional.of(nextState);
        }

        return Optional.empty();
    }

    private boolean isConditionsSatisfied() {
        return conditions.stream().allMatch(Condition::isSatisfied);
    }
}

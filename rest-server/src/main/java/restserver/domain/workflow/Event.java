package com.codeexamples.restserver.domain.workflow;

public interface Event<E extends WorkflowEntity<? extends Enum<?>>> {
    E getEntity();
}

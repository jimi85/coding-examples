package com.codeexamples.restserver.domain.workflow;

public interface StateFactory<E extends WorkflowEntity<S>, S extends Enum<S>> {
    State<E, S> createWorkflowState(E entity, Event<E> event);
}

package com.codeexamples.restserver.domain.workflow;

public interface WorkflowEntity<S extends Enum<S>> {
    S getEntityState();
    void setEntityState(S state);
}

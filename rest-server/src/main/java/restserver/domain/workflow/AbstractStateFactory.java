package com.codeexamples.restserver.domain.workflow;

public abstract class AbstractStateFactory<E extends WorkflowEntity<S>, S extends Enum<S>> implements StateFactory<E, S>{
    @Override
    public State<E, S> createWorkflowState(E entity, Event<E> event) {
        WorkflowContext<E> context = new WorkflowContext<>(entity, event.getClass().getSimpleName());
        return getState(entity.getEntityState(), context);
    }

    abstract protected State<E, S> getState(S state, WorkflowContext<E> context);
}

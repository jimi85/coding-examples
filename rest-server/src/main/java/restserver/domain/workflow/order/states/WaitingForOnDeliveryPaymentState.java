package com.codeexamples.restserver.domain.workflow.order.states;

import com.codeexamples.restserver.domain.entity.Order;
import com.codeexamples.restserver.domain.workflow.AbstractState;
import com.codeexamples.restserver.domain.workflow.Condition;
import com.codeexamples.restserver.domain.workflow.Transition;
import com.codeexamples.restserver.domain.workflow.WorkflowContext;
import com.codeexamples.restserver.domain.workflow.order.events.CompletedEvent;
import com.codeexamples.restserver.domain.workflow.order.events.NotTakenOverEvent;
import lombok.EqualsAndHashCode;

import java.util.Set;

@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
public class WaitingForOnDeliveryPaymentState extends AbstractState<Order, Order.StateEnum> {

    public WaitingForOnDeliveryPaymentState(WorkflowContext<Order> context) {
        super(context);
    }

    @Override
    @EqualsAndHashCode.Include
    public Order.StateEnum getEntityState() {
        return Order.StateEnum.WAITING_FOR_ON_DELIVERY_PAYMENT;
    }

    @Override
    public Set<Transition<Order, Order.StateEnum>> getTransitions() {
        return Set.of(
                new Transition<>(
                        new CompletedState(this.getContext()),
                        Set.of(new Condition<>(context.getEventClass(), eventClass -> CompletedEvent.class.getSimpleName().equals(eventClass)))
                ),
                new Transition<>(
                        new NotTakenOverState(this.getContext()),
                        Set.of(new Condition<>(context.getEventClass(), eventClass -> NotTakenOverEvent.class.getSimpleName().equals(eventClass)))
                )
        );
    }
}

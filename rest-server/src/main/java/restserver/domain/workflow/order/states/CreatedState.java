package com.codeexamples.restserver.domain.workflow.order.states;

import com.codeexamples.restserver.domain.entity.Order;
import com.codeexamples.restserver.domain.workflow.AbstractState;
import com.codeexamples.restserver.domain.workflow.Condition;
import com.codeexamples.restserver.domain.workflow.Transition;
import com.codeexamples.restserver.domain.workflow.WorkflowContext;
import com.codeexamples.restserver.domain.workflow.order.events.StartProcessingEvent;
import lombok.EqualsAndHashCode;

import java.util.Set;

@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
public class CreatedState extends AbstractState<Order, Order.StateEnum> {

    public CreatedState(WorkflowContext<Order> context) {
        super(context);
    }

    @Override
    @EqualsAndHashCode.Include
    public Order.StateEnum getEntityState() {
        return Order.StateEnum.CREATED;
    }

    @Override
    public Set<Transition<Order, Order.StateEnum>> getTransitions() {
        return Set.of(
                new Transition<>(
                        new ProcessingState(this.getContext()),
                        Set.of(
                                new Condition<>(context.getEntity(), order -> !order.hasOnlinePayment()),
                                new Condition<>(context.getEventClass(), eventClass -> StartProcessingEvent.class.getSimpleName().equals(eventClass))
                        )
                ),
                new Transition<>(
                        new WaitingForPaymentState(this.getContext()),
                        Set.of(new Condition<>(context.getEntity(), Order::hasOnlinePayment))
                )
        );
    }
}

package com.codeexamples.restserver.domain.workflow.order.states;

import com.codeexamples.restserver.domain.entity.Order;
import com.codeexamples.restserver.domain.workflow.AbstractState;
import com.codeexamples.restserver.domain.workflow.Condition;
import com.codeexamples.restserver.domain.workflow.Transition;
import com.codeexamples.restserver.domain.workflow.WorkflowContext;
import com.codeexamples.restserver.domain.workflow.order.events.CancelledEvent;
import com.codeexamples.restserver.domain.workflow.order.events.OnlinePaymentSuccessEvent;
import lombok.EqualsAndHashCode;

import java.util.Set;

@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
public class PaymentFailedState extends AbstractState<Order, Order.StateEnum> {

    public PaymentFailedState(WorkflowContext<Order> context) {
        super(context);
    }

    @Override
    @EqualsAndHashCode.Include
    public Order.StateEnum getEntityState() {
        return Order.StateEnum.PAYMENT_FAILED;
    }

    @Override
    public Set<Transition<Order, Order.StateEnum>> getTransitions() {
        return Set.of(
                new Transition<>(
                        new PaidState(this.getContext()),
                        Set.of(new Condition<>(context.getEventClass(), eventClass -> OnlinePaymentSuccessEvent.class.getSimpleName().equals(eventClass)))
                ),
                new Transition<>(
                        new CancelledState(this.getContext()),
                        Set.of(new Condition<>(context.getEventClass(), eventClass -> CancelledEvent.class.getSimpleName().equals(eventClass)))
                )
        );
    }
}

package com.codeexamples.restserver.domain.workflow.order.listeners;

import com.codeexamples.restserver.domain.entity.Order;
import com.codeexamples.restserver.domain.workflow.Event;
import com.codeexamples.restserver.domain.workflow.State;
import com.codeexamples.restserver.domain.workflow.StateFactory;
import com.codeexamples.restserver.domain.workflow.WorkflowEventListener;
import lombok.RequiredArgsConstructor;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class OrderWorkflowEventListener implements WorkflowEventListener<Order> {

    private final StateFactory<Order, Order.StateEnum> stateFactory;

    @Override
    @EventListener
    public void handleWorkflowEvent(Event<Order> event) {
        Order order = event.getEntity();
        State<Order, Order.StateEnum> startState = stateFactory.createWorkflowState(order, event);
        State<Order, Order.StateEnum> endState = startState.processState();
        order.setEntityState(endState.getEntityState());
    }
}

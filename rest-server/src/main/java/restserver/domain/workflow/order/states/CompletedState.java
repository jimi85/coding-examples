package com.codeexamples.restserver.domain.workflow.order.states;

import com.codeexamples.restserver.domain.entity.Order;
import com.codeexamples.restserver.domain.workflow.AbstractState;
import com.codeexamples.restserver.domain.workflow.Condition;
import com.codeexamples.restserver.domain.workflow.Transition;
import com.codeexamples.restserver.domain.workflow.WorkflowContext;
import com.codeexamples.restserver.domain.workflow.order.events.CreateComplaintEvent;
import com.codeexamples.restserver.domain.workflow.order.events.FinishedEvent;
import com.codeexamples.restserver.domain.workflow.order.events.ReturnedEvent;
import lombok.EqualsAndHashCode;

import java.util.Set;

@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
public class CompletedState extends AbstractState<Order, Order.StateEnum> {

    public CompletedState(WorkflowContext<Order> context) {
        super(context);
    }

    @Override
    @EqualsAndHashCode.Include
    public Order.StateEnum getEntityState() {
        return Order.StateEnum.COMPLETED;
    }

    public Set<Transition<Order, Order.StateEnum>> getTransitions() {
        return Set.of(
                new Transition<>(
                        new DoneState(this.getContext()),
                        Set.of(new Condition<>(context.getEventClass(), eventClass -> FinishedEvent.class.getSimpleName().equals(eventClass)))
                ),
                new Transition<>(
                        new ComplaintState(this.getContext()),
                        Set.of(new Condition<>(context.getEventClass(), eventClass -> CreateComplaintEvent.class.getSimpleName().equals(eventClass)))
                ),
                new Transition<>(
                        new ReturnedIn14DaysState(this.getContext()),
                        Set.of(new Condition<>(context.getEventClass(), eventClass -> ReturnedEvent.class.getSimpleName().equals(eventClass)))
                )
        );
    }
}

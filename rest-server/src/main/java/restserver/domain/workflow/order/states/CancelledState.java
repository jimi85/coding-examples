package com.codeexamples.restserver.domain.workflow.order.states;

import com.codeexamples.restserver.domain.entity.Order;
import com.codeexamples.restserver.domain.workflow.AbstractState;
import com.codeexamples.restserver.domain.workflow.Condition;
import com.codeexamples.restserver.domain.workflow.Transition;
import com.codeexamples.restserver.domain.workflow.WorkflowContext;
import com.codeexamples.restserver.domain.workflow.order.events.RefundedEvent;
import lombok.EqualsAndHashCode;

import java.util.Set;

@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
public class CancelledState extends AbstractState<Order, Order.StateEnum> {

    public CancelledState(WorkflowContext<Order> context) {
        super(context);
    }

    @Override
    @EqualsAndHashCode.Include
    public Order.StateEnum getEntityState() {
        return Order.StateEnum.CANCELLED;
    }

    @Override
    public Set<Transition<Order, Order.StateEnum>> getTransitions() {
        return Set.of(
                new Transition<>(
                        new DoneState(this.getContext()),
                        Set.of(new Condition<>(context.getEntity(), order -> !order.isPaid()))
                ),
                new Transition<>(
                        new RefundedState(this.getContext()),
                        Set.of(
                                new Condition<>(context.getEntity(), Order::isPaid),
                                new Condition<>(context.getEventClass(), eventClass -> RefundedEvent.class.getSimpleName().equals(eventClass))
                        )
                )
        );
    }
}

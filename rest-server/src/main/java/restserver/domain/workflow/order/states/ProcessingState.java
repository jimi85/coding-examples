package com.codeexamples.restserver.domain.workflow.order.states;

import com.codeexamples.restserver.domain.entity.Order;
import com.codeexamples.restserver.domain.workflow.AbstractState;
import com.codeexamples.restserver.domain.workflow.Condition;
import com.codeexamples.restserver.domain.workflow.Transition;
import com.codeexamples.restserver.domain.workflow.WorkflowContext;
import com.codeexamples.restserver.domain.workflow.order.events.CancelledEvent;
import com.codeexamples.restserver.domain.workflow.order.events.SentEvent;
import lombok.EqualsAndHashCode;

import java.util.Set;

@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
public class ProcessingState extends AbstractState<Order, Order.StateEnum> {

    public ProcessingState(WorkflowContext<Order> context) {
        super(context);
    }

    @Override
    @EqualsAndHashCode.Include
    public Order.StateEnum getEntityState() {
        return Order.StateEnum.PROCESSING;
    }

    @Override
    public Set<Transition<Order, Order.StateEnum>> getTransitions() {
        return Set.of(
                new Transition<>(
                        new SentState(this.getContext()),
                        Set.of(new Condition<>(context.getEventClass(), eventClass -> SentEvent.class.getSimpleName().equals(eventClass)))
                ),
                new Transition<>(
                        new CancelledState(this.getContext()),
                        Set.of(
                                new Condition<>(context.getEventClass(), eventClass -> CancelledEvent.class.getSimpleName().equals(eventClass))
                        )
                )
        );
    }
}

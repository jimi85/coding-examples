package com.codeexamples.restserver.domain.workflow.order.states;

import com.codeexamples.restserver.domain.entity.Order;
import com.codeexamples.restserver.domain.workflow.AbstractState;
import com.codeexamples.restserver.domain.workflow.Condition;
import com.codeexamples.restserver.domain.workflow.Transition;
import com.codeexamples.restserver.domain.workflow.WorkflowContext;
import com.codeexamples.restserver.domain.workflow.order.events.CompletedEvent;
import com.codeexamples.restserver.domain.workflow.order.events.NotTakenOverEvent;
import lombok.EqualsAndHashCode;

import java.util.Set;

@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
public class SentState extends AbstractState<Order, Order.StateEnum> {

    public SentState(WorkflowContext<Order> context) {
        super(context);
    }

    @Override
    @EqualsAndHashCode.Include
    public Order.StateEnum getEntityState() {
        return Order.StateEnum.SENT;
    }

    @Override
    public Set<Transition<Order, Order.StateEnum>> getTransitions() {
        return Set.of(
                new Transition<>(
                        new CompletedState(this.getContext()),
                        Set.of(
                                new Condition<>(context.getEntity(), Order::hasOnlinePayment),
                                new Condition<>(context.getEventClass(), eventClass -> CompletedEvent.class.getSimpleName().equals(eventClass))
                        )
                ),
                new Transition<>(
                        new WaitingForOnDeliveryPaymentState(this.getContext()),
                        Set.of(new Condition<>(context.getEntity(), order -> !order.hasOnlinePayment()))
                ),
                new Transition<>(
                        new NotTakenOverState(this.getContext()),
                        Set.of(
                                new Condition<>(context.getEntity(), Order::hasOnlinePayment),
                                new Condition<>(context.getEventClass(), eventClass -> NotTakenOverEvent.class.getSimpleName().equals(eventClass))
                        )
                )
        );
    }
}

package com.codeexamples.restserver.domain.workflow.order;

import com.codeexamples.restserver.domain.entity.Order;
import com.codeexamples.restserver.domain.exceptions.WorkflowException;
import com.codeexamples.restserver.domain.workflow.AbstractStateFactory;
import com.codeexamples.restserver.domain.workflow.State;
import com.codeexamples.restserver.domain.workflow.WorkflowContext;
import com.codeexamples.restserver.domain.workflow.order.states.*;
import org.springframework.stereotype.Component;

import static java.lang.String.format;

@Component
public class OrderStateFactory extends AbstractStateFactory<Order, Order.StateEnum> {

    @Override
    protected State<Order, Order.StateEnum> getState(Order.StateEnum state, WorkflowContext<Order> context) {
        if (state == null) {
            return new CreatedState(context);
        }

        switch (state) {
            case CREATED:
                return new CreatedState(context);
            case WAITING_FOR_PAYMENT:
                return new WaitingForPaymentState(context);
            case PAYMENT_FAILED:
                return new PaymentFailedState(context);
            case CANCELLED:
                return new CancelledState(context);
            case PAID:
                return new PaidState(context);
            case PROCESSING:
                return new ProcessingState(context);
            case SENT:
                return new SentState(context);
            case WAITING_FOR_ON_DELIVERY_PAYMENT:
                return new WaitingForOnDeliveryPaymentState(context);
            case COMPLETED:
                return new CompletedState(context);
            case NOT_TAKEN_OVER:
                return new NotTakenOverState(context);
            case COMPLAINT:
                return new ComplaintState(context);
            case RETURNED_IN_14_DAYS:
                return new ReturnedIn14DaysState(context);
            case RETURNED:
                return new ReturnedState(context);
            case REFUNDED:
                return new RefundedState(context);
            case DONE:
                return new DoneState(context);
            default:
                throw new WorkflowException(format("Order state %s not exist", state.name()));
        }
    }
}

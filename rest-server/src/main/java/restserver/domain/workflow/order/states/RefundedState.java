package com.codeexamples.restserver.domain.workflow.order.states;

import com.codeexamples.restserver.domain.entity.Order;
import com.codeexamples.restserver.domain.workflow.AbstractState;
import com.codeexamples.restserver.domain.workflow.Transition;
import com.codeexamples.restserver.domain.workflow.WorkflowContext;
import lombok.EqualsAndHashCode;

import java.util.Collections;
import java.util.Set;

@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
public class RefundedState extends AbstractState<Order, Order.StateEnum> {

    public RefundedState(WorkflowContext<Order> context) {
        super(context);
    }

    @Override
    @EqualsAndHashCode.Include
    public Order.StateEnum getEntityState() {
        return Order.StateEnum.REFUNDED;
    }

    @Override
    public Set<Transition<Order, Order.StateEnum>> getTransitions() {
        return Collections.emptySet();
    }
}

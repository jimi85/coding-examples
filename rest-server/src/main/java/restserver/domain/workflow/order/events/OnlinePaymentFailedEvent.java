package com.codeexamples.restserver.domain.workflow.order.events;

import com.codeexamples.restserver.domain.entity.Order;
import com.codeexamples.restserver.domain.workflow.Event;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class OnlinePaymentFailedEvent implements Event<Order> {
    private final Order order;

    @Override
    public Order getEntity() {
        return order;
    }
}

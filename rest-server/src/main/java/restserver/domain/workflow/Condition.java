package com.codeexamples.restserver.domain.workflow;

import lombok.RequiredArgsConstructor;

import java.util.function.Predicate;

@RequiredArgsConstructor
public class Condition<T> {

    protected final T testedObject;
    protected final Predicate<T> predicate;

    public boolean isSatisfied() {
        return predicate.test(testedObject);
    }
}

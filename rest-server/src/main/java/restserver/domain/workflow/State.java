package com.codeexamples.restserver.domain.workflow;

import java.util.Set;

public interface State <E extends WorkflowEntity<S>, S extends Enum<S>> {
    S getEntityState();
    Set<Transition<E, S>> getTransitions();
    State<E, S> processState();
}

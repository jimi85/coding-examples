package com.codeexamples.restserver.domain.workflow;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class WorkflowContext<E extends WorkflowEntity<? extends Enum<?>>> {
    private final E entity;
    private final String eventClass;
}

package com.codeexamples.restserver.domain.workflow;

import com.codeexamples.restserver.domain.exceptions.WorkflowException;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.util.CollectionUtils;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Getter
public abstract class AbstractState<E extends WorkflowEntity<S>, S extends Enum<S>> implements State<E, S> {

    protected final WorkflowContext<E> context;
    @Override
    public State<E, S> processState() {
        Set<Transition<E, S>> transitions = this.getTransitions();
        if (CollectionUtils.isEmpty(transitions)) {
            return this;
        }

        Optional<State<E, S>> nextState = this.getNextState(transitions);
        if (nextState.isPresent()) {
            return nextState.get().processState();
        }

        return this;
    }

    protected Optional<State<E, S>> getNextState(Set<Transition<E, S>> transitions) {
        Set<State<E, S>> nextStates = transitions.stream()
                .map(Transition::getNextState)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toSet());

        if (nextStates.size() > 1) {
            throw new WorkflowException("State " + this.getClass().getSimpleName() + " has more than one possible descendants");
        }

        return nextStates.stream().findFirst();
    }
}

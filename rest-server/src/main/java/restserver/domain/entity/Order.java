package com.codeexamples.restserver.domain.entity;

import com.codeexamples.restserver.domain.workflow.WorkflowEntity;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Entity
@Table(name = "orders")
@Data
public class Order implements WorkflowEntity<Order.StateEnum> {

    public static final String NOT_FOUND_MESSAGE = "Order not found";

    public Order() {
        if (extId == null) {
            extId = UUID.randomUUID();
        }
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false)
    private Long id;

    @NotNull
    @Column(name = "ext_id", unique = true, updatable = false)
    @Type(type = "uuid-char")
    private UUID extId;

    @Column(name = "code", nullable = false, unique = true, updatable = false)
    private String code;

    @Enumerated(EnumType.STRING)
    @Column(name = "state", nullable = false)
    private StateEnum state;

    @Column(name = "customer_note", updatable = false)
    @Type(type="text")
    private String customerNote;

    @Column(name = "admin_note")
    @Type(type="text")
    private String adminNote;

    @Column(name = "client_security_info")
    @Type(type="text")
    private String clientSecurityInformation;

    @Column(name = "paid")
    private boolean paid = false;

    @CreationTimestamp
    @Column(name = "created_at")
    private ZonedDateTime createdAt;

    @UpdateTimestamp
    @Column(name = "modified_at")
    private ZonedDateTime modifiedAt;

    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<OrderItem> items = new ArrayList<>();

    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<OrderAddress> addresses = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "eshop_id", nullable = false, updatable = false)
    private Eshop eshop;

    public void setItems(List<OrderItem> orderItems) {
        orderItems.forEach(item -> item.setOrder(this));
        this.items.addAll(orderItems);
    }

    public void setAddresses(List<OrderAddress> addresses) {
        addresses.forEach(item -> item.setOrder(this));
        this.addresses.addAll(addresses);
    }

    public BigDecimal getTotalPriceWithVat() {
        return items.stream()
                .map(i -> i.getPriceWithVat().multiply(BigDecimal.valueOf(i.getCount())))
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public Optional<OrderAddress> getAddress(UUID addressExtId) {
        return addresses.stream()
                .filter(i -> i.getExtId().equals(addressExtId))
                .findAny();
    }

    public Optional<OrderItem> getItemByProductExtId(UUID productExtId) {
        return items.stream()
                .filter(OrderItem::isProduct)
                .filter(i -> i.getProduct().getExtId().equals(productExtId))
                .findFirst();
    }

    public boolean hasBillingAddress() {
        return addresses.stream().anyMatch(OrderAddress::isBilling);
    }

    public boolean hasOnlinePayment() {
        return items.stream().anyMatch(OrderItem::isOnlinePayment);
    }

    public enum StateEnum {
        CREATED,
        WAITING_FOR_PAYMENT,
        PAYMENT_FAILED,
        PAID,
        PROCESSING,
        SENT,
        WAITING_FOR_ON_DELIVERY_PAYMENT,
        NOT_TAKEN_OVER,
        RETURNED,
        COMPLETED,
        COMPLAINT,
        RETURNED_IN_14_DAYS,
        DONE,
        CANCELLED,
        REFUNDED
    }

    @Override
    public StateEnum getEntityState() {
        return getState();
    }

    @Override
    public void setEntityState(StateEnum state) {
        setState(state);
    }
}

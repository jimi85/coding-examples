package com.codeexamples.restserver.utils;

import com.codeexamples.restserver.controllers.FilesController;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

public class FileUtils {

    public final static String MINIATURE_PREFIX = "miniature";
    public final static String DETAIL_PREFIX = "detail";

    public static boolean isImage(MultipartFile file) {
        return file.getContentType() != null && file.getContentType().startsWith("image/");
    }

    public static String createMiniatureFileName(String fileName) {
        return StringUtils.join(MINIATURE_PREFIX, "-", fileName);
    }

    public static String createDetailFileName(String fileName) {
        return StringUtils.join(DETAIL_PREFIX, "-", fileName);
    }

    public static HashMap<String, String> getAllFilePaths(String type, String id, String fileName) {
        HashMap<String, String> paths = new HashMap<>();
        paths.put("original", linkTo(methodOn(FilesController.class).getFile(type, id, fileName)).toString());
        paths.put("miniature", linkTo(methodOn(FilesController.class).getFile(type, id, createMiniatureFileName(fileName))).toString());
        paths.put("detail", linkTo(methodOn(FilesController.class).getFile(type, id, createDetailFileName(fileName))).toString());
        return paths;
    }
}

package com.codeexamples.restserver.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class PriceUtils {

    public static BigDecimal getPriceWithVat(BigDecimal priceWithoutVat, BigDecimal vat) {
        return priceWithoutVat.multiply(
                BigDecimal.valueOf(10000, 2).add(vat).divide(BigDecimal.valueOf(10000, 2), RoundingMode.HALF_UP)
        );
    }

    public static Long getPriceInSmallestCoinValue(BigDecimal value) {
        return value.multiply(BigDecimal.valueOf(100)).longValue();
    }
}

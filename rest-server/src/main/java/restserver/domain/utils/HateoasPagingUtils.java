package com.codeexamples.restserver.utils;

import org.springframework.data.domain.Page;
import org.springframework.hateoas.PagedModel;

public class HateoasPagingUtils {
    public static PagedModel.PageMetadata getPageMetadata(Page<?> page) {
        return new PagedModel.PageMetadata(
                page.getPageable().getPageSize(),
                page.getPageable().getPageNumber(),
                page.getTotalElements(),
                (long)Math.ceil((double)page.getTotalElements()/page.getPageable().getPageSize())
        );
    }
}

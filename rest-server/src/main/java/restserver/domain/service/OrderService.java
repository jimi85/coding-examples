package com.codeexamples.restserver.domain.service;

import com.codeexamples.restserver.domain.dto.*;
import com.codeexamples.restserver.domain.entity.*;
import com.codeexamples.restserver.domain.exceptions.NotFoundException;
import com.codeexamples.restserver.domain.mappers.AddressMapper;
import com.codeexamples.restserver.domain.mappers.OrderMapper;
import com.codeexamples.restserver.domain.repository.OrderRepository;
import com.codeexamples.restserver.domain.repository.projections.OrderItemProductAggregationProjection;
import com.codeexamples.restserver.domain.utils.PagingUtils;
import com.codeexamples.restserver.domain.utils.SearchUtils;
import com.codeexamples.restserver.domain.validators.BasketValidator;
import com.codeexamples.restserver.domain.validators.OrderValidator;
import com.codeexamples.restserver.domain.workflow.order.events.*;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Transactional
public class OrderService {

    @Value("${startup.pagination.orders.page-size}")
    private Integer pageSize;
    @Value("${startup.dashboard.orders.latest-size}")
    private Integer dashboardSize;
    @Value("${startup.dashboard.products.best-sellers-size}")
    private Integer bestSellersSize;

    private final ApplicationEventPublisher eventPublisher;
    private final OrderRepository orderRepository;

    private final EshopService eshopService;
    private final OrderMapper orderMapper;
    private final BasketService basketService;
    private final BasketValidator basketValidator;
    private final ProductService productService;
    private final AddressMapper addressMapper;
    private final OrderValidator orderValidator;

    public OrderDto createOrder(OrderPostDto orderPostDto) {
        UUID basketExtId = orderPostDto.getBasketExtId();
        UUID eshopExtId = orderPostDto.getEshopExtId();
        Basket basket = basketService.getDomainBasket(basketExtId, eshopExtId);
        basketValidator.validateCreateOrder(basket);

        Order order = orderMapper.basketToOrder(basket);
        order.setEshop(basket.getEshop());
        order.setCode(getOrderCode(basket.getEshop()));
        order.setClientSecurityInformation(orderPostDto.getClientSecurityInformation());

        OrderItem transport = orderMapper.transportToOrderItem(basket.getTransport());
        transport.setOrder(order);
        transport.setTransport(basket.getTransport());
        OrderItem payment = orderMapper.paymentToOrderItem(basket.getPayment());
        payment.setOrder(order);
        payment.setPayment(basket.getPayment());
        order.getItems().add(transport);
        order.getItems().add(payment);
        order.getItems().forEach(orderItem -> {
            orderItem.setEshop(basket.getEshop());
            if (orderItem.isProduct()) {
                productService.reduceProductStock(orderItem.getProduct().getExtId(), eshopExtId, orderItem.getCount());
            }
        });

        basketService.deleteBasket(basket);
        eventPublisher.publishEvent(new CreatedEvent(order));

        order = orderRepository.saveAndFlush(order);
        return orderMapper.orderToOrderDto(order);
    }

    private String getOrderCode(Eshop eshop) {
        ZonedDateTime now = ZonedDateTime.now();
        long orderCount = orderRepository.countByEshopAndCreatedAtBetween(eshop, now.with(LocalTime.MIN), now.with(LocalTime.MAX));

        return StringUtils.join(
                LocalDate.now().format(DateTimeFormatter.BASIC_ISO_DATE),
                StringUtils.leftPad(String.valueOf(orderCount+1), 4, "0")
        );
    }

    public OrderDto getOrder(UUID orderExtId, UUID eshopExtId) {
        Order order = getDomainOrder(orderExtId, eshopExtId);
        return orderMapper.orderToOrderDto(order);
    }

    public OrderDto getOwnerOrder(UUID orderExtId, UUID eshopExtId) {
        Order order = getDomainOwnerOrder(orderExtId, eshopExtId);
        return orderMapper.orderToOrderDto(order);
    }

    private Order getDomainOrder(UUID orderExtId, UUID eshopExtId) {
        return orderRepository.findByExtIdAndEshop(orderExtId, eshopService.getDomainEshop(eshopExtId)).orElseThrow(() -> new NotFoundException(Order.NOT_FOUND_MESSAGE));
    }

    private Order getDomainOwnerOrder(UUID orderExtId, UUID eshopExtId) {
        return orderRepository.findByExtIdAndEshop(orderExtId, eshopService.getDomainOwnerEshop(eshopExtId)).orElseThrow(() -> new NotFoundException(Order.NOT_FOUND_MESSAGE));
    }

    public Page<OrderDto> getOrders(String search, Integer page, String sort, UUID eshopId) {
        Eshop eshop = eshopService.getDomainOwnerEshop(eshopId);
        return orderRepository.findAll(
                SearchUtils.getOrderBuilder(search, eshop.getId()).build(),
                PagingUtils.createPageRequest(page, pageSize, sort)
        ).map(orderMapper::orderToOrderDto);
    }

    .....

    public OrderDto processManualAction(UUID orderExtId, UUID eshopExtId, OrderAction action) {
        Order order = getDomainOwnerOrder(orderExtId, eshopExtId);
        switch (action) {
            case CANCEL:
                cancelOrder(order);
                break;
            case COMPLETE:
                eventPublisher.publishEvent(new CompletedEvent(order));
                break;
            case CREATE_COMPLAINT:
                eventPublisher.publishEvent(new CreateComplaintEvent(order));
                break;
            case FINISH:
                eventPublisher.publishEvent(new FinishedEvent(order));
                break;
            case NOT_TAKE_OVER:
                eventPublisher.publishEvent(new NotTakenOverEvent(order));
                break;
            case REFUND:
                eventPublisher.publishEvent(new RefundedEvent(order));
                break;
            case RETURN:
                eventPublisher.publishEvent(new ReturnedEvent(order));
                break;
            case SENT:
                eventPublisher.publishEvent(new SentEvent(order));
                break;
            case START_PROCESSING:
                eventPublisher.publishEvent(new StartProcessingEvent(order));
                break;

        }
        return orderMapper.orderToOrderDto(order);
    }
}

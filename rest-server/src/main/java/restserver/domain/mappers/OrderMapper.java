package com.codeexamples.restserver.domain.mappers;

import com.codeexamples.restserver.domain.dto.*;
import com.codeexamples.restserver.domain.entity.*;
import com.codeexamples.restserver.domain.repository.projections.OrderItemProductAggregationProjection;
import com.codeexamples.restserver.utils.PriceUtils;
import com.codeexamples.utils.mappers.UUIDMapper;
import org.mapstruct.*;

import java.util.List;

@Mapper(
        implementationName = "OrderDomainMapper",
        componentModel = "spring",
        uses = {UUIDMapper.class},
        imports = {PriceUtils.class},
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT,
        nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS,
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE
)
public interface OrderMapper {

    List<OrderDto> ordersToOrderDtos(List<Order> orders);

    @Mapping(target = "eshopExtId", source = "eshop.extId")
    @Mapping(target = "onlinePaid", expression = "java(order.hasOnlinePayment())")
    OrderDto orderToOrderDto(Order order);

    @Mapping(target = "eshopExtId", source = "eshop.extId")
    AddressDto orderAddressToAddressDto(OrderAddress address);

    @Mapping(target = "eshopExtId", source = "eshop.extId")
    @Mapping(target = "productExtId", source = "product.extId")
    @Mapping(target = "paymentExtId", source = "payment.extId")
    @Mapping(target = "transportExtId", source = "transport.extId")
    @Mapping(target = "price.priceWithoutVat", source = "priceWithoutVat")
    @Mapping(target = "price.priceWithVat", source = "priceWithVat")
    @Mapping(target = "price.vat", source = "vat")
    @Mapping(target = "totalPrice.priceWithoutVat", source = "totalPriceWithoutVat")
    @Mapping(target = "totalPrice.priceWithVat", source = "totalPriceWithVat")
    @Mapping(target = "totalPrice.vat", source = "vat")
    OrderItemDto orderItemToOrderItemDto(OrderItem orderItem);

    List<OrderItemProductAggregationDto> aggregationsToOrderItemDtos(List<OrderItemProductAggregationProjection> aggregations);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "extId", ignore = true)
    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "modifiedAt", ignore = true)
    @Mapping(target = "state", ignore = true)
    Order basketToOrder(Basket basket);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "extId", ignore = true)
    @Mapping(target = "name", source = "product.name")
    @Mapping(target = "priceWithoutVat", source = "product.price.priceWithoutVat")
    @Mapping(target = "priceWithVat", source = "product.price.priceWithVat")
    @Mapping(target = "vat", source = "product.price.vat")
    @Mapping(target = "type", constant = "PRODUCT")
    OrderItem basketItemToOrderItem(BasketItem basketItem);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "extId", ignore = true)
    @Mapping(target = "priceWithoutVat", source = "price.priceWithoutVat")
    @Mapping(target = "priceWithVat", source = "price.priceWithVat")
    @Mapping(target = "vat", source = "price.vat")
    @Mapping(target = "type", constant = "TRANSPORT")
    OrderItem transportToOrderItem(Transport transport);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "extId", ignore = true)
    @Mapping(target = "priceWithoutVat", source = "price.priceWithoutVat")
    @Mapping(target = "priceWithVat", source = "price.priceWithVat")
    @Mapping(target = "vat", source = "price.vat")
    @Mapping(target = "type", constant = "PAYMENT")
    OrderItem paymentToOrderItem(Payment payment);

    @Mapping(target = "type", constant = "PRODUCT")
    OrderItem postDtoToProductOrderItem(OrderItemPostDto dto);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "extId", ignore = true)
    OrderAddress addressToOrderAddress(Address address);

    List<OrderAddress> adressesToOrderAddresses(List<Address> addresses);
}

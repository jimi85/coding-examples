package com.codeexamples.restserver.domain.repository.projections;

import java.math.BigDecimal;

public interface OrderItemProductAggregationProjection {
    String getEshopExtId();
    String getProductExtId();
    String getName();
    Integer getCount();
    BigDecimal getTotalPrice();
}

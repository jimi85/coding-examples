package com.codeexamples.restserver.domain.repository;

import com.codeexamples.restserver.domain.entity.Eshop;
import com.codeexamples.restserver.domain.entity.Order;
import com.codeexamples.restserver.domain.entity.Payment;
import com.codeexamples.restserver.domain.repository.projections.OrderItemProductAggregationProjection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long>, JpaSpecificationExecutor<Order> {
    Optional<Order> findByExtIdAndEshop(UUID extId, Eshop eshop);
    long countByEshopAndCreatedAtBetween(Eshop eshop, ZonedDateTime from, ZonedDateTime to);
    List<Order> findAllByPaidIsFalseAndStateIsInAndItems_Payment_TypeIsIn(Set<Order.StateEnum> states, Set<Payment.PaymentType> type);
    List<Order> findAllByEshopIsIn(List<Eshop> eshops);
    List<Order> findAllByEshop(Eshop eshop);
    Page<Order> findAllByEshopIsIn(List<Eshop> eshops, Pageable pageable);
    Page<Order> findAllByEshop(Eshop eshop, Pageable pageable);

    @Query(value = "SELECT e.ext_id as eshopExtId, p.ext_id as productExtId, i.name, SUM(count) AS count, SUM(count)*price_with_vat AS totalPrice " +
            "FROM order_items AS i JOIN eshops AS e ON e.id = i.eshop_id JOIN products AS p ON i.product_id = p.id WHERE i.eshop_id = :eshopId AND i.type = 'PRODUCT' " +
            "GROUP BY productExtId ORDER BY totalPrice DESC, count DESC LIMIT 0, :limit", nativeQuery = true)
    List<OrderItemProductAggregationProjection> findAllProductAggregationTotalSold(Long eshopId, Integer limit);
}

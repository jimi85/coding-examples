package com.codeexamples.restserver.domain.dto;

import lombok.Value;

import java.util.UUID;

@Value
public class OrderPostDto {
    UUID basketExtId;
    UUID eshopExtId;
    String clientSecurityInformation;
}

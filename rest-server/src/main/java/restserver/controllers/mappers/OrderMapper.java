package com.codeexamples.restserver.controllers.mappers;

import com.codeexamples.restserver.controllers.requests.OrderItemPostRequest;
import com.codeexamples.restserver.controllers.requests.OrderItemPutRequest;
import com.codeexamples.restserver.controllers.requests.OrderPostRequest;
import com.codeexamples.restserver.controllers.requests.OrderPutRequest;
import com.codeexamples.restserver.controllers.resources.OrderAdminResource;
import com.codeexamples.restserver.controllers.resources.OrderItemResource;
import com.codeexamples.restserver.controllers.resources.OrderPublicResource;
import com.codeexamples.restserver.domain.dto.*;
import com.codeexamples.restserver.exceptions.NotFoundException;
import com.codeexamples.restserver.libraries.onlinepayment.controller.OnlinePaymentsController;
import com.codeexamples.restserver.libraries.onlinepayment.dto.CreateGoPayPaymentDto;
import com.codeexamples.restserver.libraries.onlinepayment.dto.Currency;
import com.codeexamples.restserver.libraries.onlinepayment.dto.GoPayPaymentDto;
import com.codeexamples.restserver.utils.PriceUtils;
import com.codeexamples.utils.mappers.UUIDMapper;
import org.mapstruct.*;
import org.springframework.data.domain.Page;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@Mapper(
        componentModel = "spring",
        imports = {PriceUtils.class},
        uses = {UUIDMapper.class, AddressMapper.class},
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT,
        nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS,
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.SET_TO_DEFAULT
)
public interface OrderMapper {

    @Mapping(target = "id", source = "extId")
    @Mapping(target = "eshopId", source = "eshopExtId")
    OrderPublicResource toPublicResource(OrderDto dto);

    @Mapping(target = "id", source = "dto.extId")
    @Mapping(target = "eshopId", source = "dto.eshopExtId")
    @Mapping(target = "state", source = "dto.state")
    OrderPublicResource toPublicResource(OrderDto dto, GoPayPaymentDto paymentDto);

    @Mapping(target = "id", source = "extId")
    @Mapping(target = "eshopId", source = "eshopExtId")
    OrderAdminResource toAdminResource(OrderDto dto);

    @Mapping(target = "id", source = "extId")
    @Mapping(target = "eshopId", source = "eshopExtId")
    @Mapping(target = "productId", source = "productExtId")
    @Mapping(target = "paymentId", source = "paymentExtId")
    @Mapping(target = "transportId", source = "transportExtId")
    OrderItemResource toItemResource(OrderItemDto dto);

    @Mapping(target = "basketExtId", source = "request.basketId")
    @Mapping(target = "eshopExtId", source = "eshopId")
    OrderPostDto toPostDto(OrderPostRequest request, String eshopId);

    List<OrderAdminResource> toAdminResources(Page<OrderDto> dtos);

    @Mapping(target = "extId", source = "orderId")
    @Mapping(target = "eshopExtId", source = "eshopId")
    OrderPutDto toPutDto(OrderPutRequest request, String orderId, String eshopId);

    @Mapping(target = "extId", source = "orderItemId")
    @Mapping(target = "orderExtId", source = "orderId")
    @Mapping(target = "eshopExtId", source = "eshopId")
    OrderItemPutDto toItemPutDto(OrderItemPutRequest request, String orderItemId, String orderId, String eshopId);

    @Mapping(target = "orderExtId", source = "orderId")
    @Mapping(target = "eshopExtId", source = "eshopId")
    @Mapping(target = "productExtId", source = "request.productId")
    OrderItemPostDto toItemPostDto(OrderItemPostRequest request, String orderId, String eshopId);

    default CreateGoPayPaymentDto toCreateGoPayDto(
            OrderDto order,
            CreateGoPayPaymentDto.Payer.PaymentMethod paymentMethod,
            Currency currency,
            CreateGoPayPaymentDto.Lang language
    ) {
        AddressDto billingAddress = Optional.ofNullable(order.getAddresses()).orElse(Collections.emptyList()).stream()
                .filter(address -> AddressDto.AddressType.BILLING.equals(address.getType()))
                .findFirst()
                .orElseThrow(() -> new NotFoundException("Missing billing address"));

        return new CreateGoPayPaymentDto(
                order.getExtId(), order.getEshopExtId(),
                order.getCode(),
                PriceUtils.getPriceInSmallestCoinValue(order.getTotalPriceWithVat()),
                currency,
                order.getCustomerNote(),
                toPaymentItems(order.getItems()),
                linkTo(OnlinePaymentsController.class).slash("thanks").toUri().toString(),
                linkTo(OnlinePaymentsController.class).slash("notify").toUri().toString(),
                new CreateGoPayPaymentDto.Payer(
                        paymentMethod,
                        billingAddress.getFirstName(), billingAddress.getSurname()
                ),
                language
        );
    }

    List<CreateGoPayPaymentDto.OrderItem> toPaymentItems(List<OrderItemDto> dtos);

    @Mapping(target = "amount", expression = "java(PriceUtils.getPriceInSmallestCoinValue(dto.getTotalPrice().getPriceWithVat()))")
    CreateGoPayPaymentDto.OrderItem toPaymentItem(OrderItemDto dto);

}

package com.codeexamples.restserver.controllers;

import com.codeexamples.restserver.controllers.requests.OrderItemPostRequest;
import com.codeexamples.restserver.controllers.requests.OrderItemPutRequest;
import com.codeexamples.restserver.controllers.requests.OrderPutRequest;
import com.codeexamples.restserver.controllers.resources.OrderAdminResource;
import com.codeexamples.restserver.services.OrdersControllerService;
import com.codeexamples.utils.validations.constraints.UUID;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.PagedModel;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import static com.codeexamples.security.config.ApiPaths.SECURED_PATH;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = SECURED_PATH + "/eshops", produces = MediaTypes.HAL_JSON_VALUE)
@Validated
@Tag(name = "Orders", description = "Admin and public endpoints for orders' operation")
public class AdminOrdersController {

    private final OrdersControllerService ordersControllerService;

    @GetMapping("{eshopId}/orders")
    @Operation(summary = "List orders")
    public PagedModel<OrderAdminResource> getOrders(
            @Parameter(schema = @Schema(format = "uuid"), required = true) @PathVariable @UUID String eshopId,
            @RequestParam(value = "search", required = false) String search,
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "sort", required = false) String sort
    ) {
        return ordersControllerService.getAdminOrders(search, page, sort, eshopId);
    }

    @PostMapping("{eshopId}/orders/{orderId}/items")
    @Operation(summary = "Order item's create")
    public EntityModel<OrderAdminResource> createOrderItem(
            @Parameter(schema = @Schema(format = "uuid"), required = true) @PathVariable @UUID String eshopId,
            @Parameter(schema = @Schema(format = "uuid"), required = true) @PathVariable @UUID String orderId,
            @RequestBody @Validated OrderItemPostRequest orderItemPostRequest
            ) {
        return ordersControllerService.createOrderItem(orderItemPostRequest, orderId, eshopId);
    }

    @PostMapping("{eshopId}/orders/{orderId}/process-workflow/{action}")
    @Operation(summary = "Order process workflow actions")
    public EntityModel<OrderAdminResource> processWorkflow(
            @Parameter(schema = @Schema(format = "uuid"), required = true) @PathVariable @UUID String eshopId,
            @Parameter(schema = @Schema(format = "uuid"), required = true) @PathVariable @UUID String orderId,
            @Parameter(example = "CANCEL, COMPLETE, CREATE_COMPLAINT, FINISH, NOT_TAKE_OVER, REFUND, RETURN, SENT, START_PROCESSING",
                    description = "This attribute is case insensitive",
                    required = true) @PathVariable String action
            ) {
        return ordersControllerService.processAction(orderId, eshopId, action);
    }
}

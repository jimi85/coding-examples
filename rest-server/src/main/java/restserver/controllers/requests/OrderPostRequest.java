package com.codeexamples.restserver.controllers.requests;

import com.codeexamples.utils.validations.constraints.UUID;
import lombok.Value;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Value
public class OrderPostRequest {
    @NotNull
    @UUID
    String basketId;

    @NotEmpty
    String clientSecurityInformation;
}

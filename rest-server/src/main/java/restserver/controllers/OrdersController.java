package com.codeexamples.restserver.controllers;

import com.codeexamples.restserver.controllers.requests.OrderPostRequest;
import com.codeexamples.restserver.controllers.resources.OrderPublicResource;
import com.codeexamples.restserver.services.OrdersControllerService;
import com.codeexamples.utils.validations.constraints.UUID;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.MediaTypes;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import static com.codeexamples.security.config.ApiPaths.PUBLIC_PATH;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = PUBLIC_PATH + "/eshops", produces = MediaTypes.HAL_JSON_VALUE)
@Validated
@Tag(name = "Orders", description = "Admin and public endpoints for orders' operation")
public class OrdersController {

    private final OrdersControllerService ordersControllerService;

    @PostMapping("{eshopId}/orders")
    @Operation(summary = "Create order")
    public EntityModel<OrderPublicResource> createOrder(
            @Parameter(schema = @Schema(format = "uuid"), required = true) @PathVariable @UUID String eshopId,
            @RequestBody(required = false) @Validated OrderPostRequest request
    ) {
        return ordersControllerService.createOrder(request, eshopId);
    }

    @GetMapping("{eshopId}/orders/{orderId}")
    @Operation(summary = "Get order")
    public EntityModel<OrderPublicResource> getOrder(
            @Parameter(schema = @Schema(format = "uuid"), required = true) @PathVariable @UUID String eshopId,
            @Parameter(schema = @Schema(format = "uuid"), required = true) @PathVariable @UUID String orderId
    ) {
        return ordersControllerService.getOrder(orderId, eshopId);
    }
}

package com.codeexamples.restserver.controllers.resources;

import com.codeexamples.restserver.controllers.OrdersController;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.Value;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.core.Relation;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Value
@EqualsAndHashCode(callSuper = true)
@ToString
@Relation(value = "order", collectionRelation = "orders")
public class OrderPublicResource extends OrderResource<OrderPublicResource> {

    public OrderPublicResource(UUID id, UUID eshopId, String code, String state, String customerNote, BigDecimal totalPriceWithVat, ZonedDateTime createdAt, List<OrderItemResource> items, List<AddressResource> addresses, String gatewayUrl) {
        super(id, eshopId, code, state, customerNote, totalPriceWithVat, createdAt, items, addresses, gatewayUrl);
    }

    @Override
    protected Set<Link> createLinks() {
        return Set.of(
                linkTo(methodOn(OrdersController.class).getOrder(this.getEshopId().toString(), this.getId().toString())).withSelfRel(),
                Link.of(gatewayUrl).withRel("GoPayGateway")
        );
    }
}

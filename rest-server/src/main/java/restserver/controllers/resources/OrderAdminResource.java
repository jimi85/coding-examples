package com.codeexamples.restserver.controllers.resources;

import com.codeexamples.restserver.controllers.AdminOrdersController;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.Value;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.core.Relation;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Value
@EqualsAndHashCode(callSuper = true)
@ToString
@Relation(value = "order", collectionRelation = "orders")
public class OrderAdminResource extends OrderResource<OrderAdminResource> {

    String adminNote;
    String clientSecurityInformation;

    public OrderAdminResource(UUID id, UUID eshopId, String code, String state, String customerNote, BigDecimal totalPriceWithVat, ZonedDateTime createdAt, List<OrderItemResource> items, List<AddressResource> addresses, String gatewayUrl, String adminNote, String clientSecurityInformation) {
        super(id, eshopId, code, state, customerNote, totalPriceWithVat, createdAt, items, addresses, gatewayUrl);
        this.adminNote = adminNote;
        this.clientSecurityInformation = clientSecurityInformation;
    }

    @Override
    protected Set<Link> createLinks() {
        return Set.of(
                linkTo(methodOn(AdminOrdersController.class).getOrder(this.getEshopId().toString(), this.getId().toString())).withSelfRel(),
                linkTo(methodOn(AdminOrdersController.class).getOrders(this.getEshopId().toString(), null, null, null)).withRel("orders")
        );
    }
}

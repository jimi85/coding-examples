package com.codeexamples.restserver.controllers.resources;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.RepresentationModel;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Getter
@EqualsAndHashCode(callSuper = false)
@ToString
public abstract class OrderResource<T extends OrderResource<? extends T>> extends RepresentationModel<T> {
    UUID id;
    UUID eshopId;
    String code;
    String state;
    String customerNote;
    BigDecimal totalPriceWithVat;
    ZonedDateTime createdAt;
    List<OrderItemResource> items;
    List<AddressResource> addresses;
    @JsonIgnore
    String gatewayUrl;

    public OrderResource(
            UUID id,
            UUID eshopId,
            String code,
            String state,
            String customerNote,
            BigDecimal totalPriceWithVat,
            ZonedDateTime createdAt,
            List<OrderItemResource> items,
            List<AddressResource> addresses,
            String gatewayUrl
    ) {
        this.id = id;
        this.eshopId = eshopId;
        this.code = code;
        this.state = state;
        this.customerNote = customerNote;
        this.totalPriceWithVat = totalPriceWithVat;
        this.createdAt = createdAt;
        this.items = items;
        this.addresses = addresses;
        this.gatewayUrl = gatewayUrl;
        this.add(createLinks());
    }

    protected abstract Set<Link> createLinks();
}

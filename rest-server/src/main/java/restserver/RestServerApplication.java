package com.codeexamples.restserver;

import com.codeexamples.security.EnableStartupSecurity;
import com.codeexamples.utils.EnableStartupUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
@EnableStartupSecurity
@EnableStartupUtils
public class RestServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(RestServerApplication.class, args);
    }

}

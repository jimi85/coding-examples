package com.codeexamples.analyzer.controllers.view;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class GraphController {

    @GetMapping("graph")
    public String graph() {
        return "graph";
    }
}

package com.codeexamples.analyzer.controllers.rest.resources;

import com.codeexamples.analyzer.controllers.rest.commons.IndicatorType;
import lombok.Value;

import java.util.List;

@Value
public class ChartResource {
    boolean zoomable;
    List<IndicatorResource> indicators;

    @Value
    public static class IndicatorResource {
        String id;
        String name;
        IndicatorType type;
        List<Object[]> data;
    }
}

package com.codeexamples.analyzer.controllers.rest;

import com.codeexamples.analyzer.controllers.rest.requests.ChartRequest;
import com.codeexamples.analyzer.controllers.rest.requests.SearchCandlesRequest;
import com.codeexamples.analyzer.controllers.rest.resources.ChartResource;
import com.codeexamples.analyzer.domain.dtos.CandleDto;
import com.codeexamples.analyzer.services.ChartsControllerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("charts")
@Slf4j
public class ChartsController {
    private final ChartsControllerService service;

    @PostMapping("search-indicators")
    public ChartResource getChartIndicators(@Valid @RequestBody ChartRequest request) {
        return service.getChartIndicators(request);
    }
}

package com.codeexamples.analyzer.controllers.rest.requests;

import com.codeexamples.analyzer.controllers.rest.commons.IndicatorType;
import com.codeexamples.analyzer.controllers.rest.commons.Interval;
import lombok.Value;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@Value
public class ChartRequest {
    @NotNull
    String symbol;
    @NotNull
    LocalDateTime from;
    @NotNull
    LocalDateTime to;
    @NotNull
    Interval interval;
    @Valid
    @NotNull
    List<IndicatorRequest> indicators;
    @NotNull
    boolean zoomed;

    @Value
    public static class IndicatorRequest {
        @NotBlank
        String id;
        @NotNull
        IndicatorType type;
        Map<String, Integer> parameters;
    }
}

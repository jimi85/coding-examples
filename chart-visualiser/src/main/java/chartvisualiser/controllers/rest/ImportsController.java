package com.codeexamples.analyzer.controllers.rest;

import com.codeexamples.analyzer.services.ImportsControllerService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("imports")
@RequiredArgsConstructor
public class ImportsController {
    private final ImportsControllerService service;

    @PostMapping("import-quotes")
    @ResponseStatus(HttpStatus.CREATED)
    public void importQuotes() {
        service.importQuotesToDb();
    }
}

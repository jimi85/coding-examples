package com.codeexamples.analyzer.services.indicators;

import com.codeexamples.analyzer.controllers.rest.commons.IndicatorType;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class SimpleMovingAverageIndicator implements Indicator {
    @Override
    public boolean isApplicable(IndicatorType indicatorType, Map<String, Integer> parameters) {
        return IndicatorType.SMA.equals(indicatorType) && parameters.containsKey("period");
    }

    @Override
    public List<Object[]> calculateData(List<Object[]> candles, LocalDateTime from, int groupingSize, Map<String, Integer> parameters) {
        int period = parameters.get("period");
        List<BigDecimal> periodCloses = new ArrayList<>();
        List<Object[]> smaData = new ArrayList<>();
        if (period == 0) {
            return smaData;
        }
        Object[] sma = new Object[2];
        int smaAverageDivider = groupingSize;
        for (int i = 0; i < candles.size(); i++) {
            Object[] candle = candles.get(i);
            periodCloses.add((BigDecimal) candle[4]);

            if (groupingSize <= 1) {
                if ((i+1) >= period) {
                    sma = new Object[2];
                    sma[0] = candle[0];
                    sma[1] = countSma(periodCloses, period);
                    smaData.add(sma);
                    periodCloses.remove(0);
                }
            } else if (Math.ceil((i+1)/(double)groupingSize) >= Math.ceil(period/(double)groupingSize)) {
                if (i%groupingSize == 0) {
                    sma = new Object[2];
                    sma[0] = candle[0];
                    if ((i+1) >= period) {
                        sma[1] = countSma(periodCloses, period);
                        periodCloses.remove(0);
                    } else {
                        sma[1] = BigDecimal.ZERO;
                        smaAverageDivider--;
                    }
                    continue;
                }

                if ((i+1) >= period) {
                    sma[1] = ((BigDecimal) sma[1]).add(countSma(periodCloses, period));
                    periodCloses.remove(0);
                } else {
                    sma[1] = BigDecimal.ZERO;
                    smaAverageDivider--;
                }

                if ((i+1)%groupingSize == 0) {
                    sma[1] = ((BigDecimal) sma[1]).divide(BigDecimal.valueOf(smaAverageDivider), RoundingMode.HALF_UP);
                    smaAverageDivider = groupingSize;
                    smaData.add(sma);
                }
            }
        }

        smaData.removeIf(s -> ((BigInteger)s[0]).longValue() < from.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());

        return smaData;
    }

    private BigDecimal countSma(List<BigDecimal> periodCloses, int period) {
        return periodCloses.stream()
                .reduce(BigDecimal.ZERO, BigDecimal::add)
                .divide(BigDecimal.valueOf(period), RoundingMode.HALF_UP);
    }

    @Override
    public String getName(Map<String, Integer> parameters) {
        return String.format("%s %d", IndicatorType.SMA, parameters.get("period"));
    }

    @Override
    public int getZoomExtraCandlesCount(Map<String, Integer> parameters) {
        return parameters.get("period");
    }
}

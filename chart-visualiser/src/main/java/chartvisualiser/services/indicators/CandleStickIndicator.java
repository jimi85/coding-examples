package com.codeexamples.analyzer.services.indicators;

import com.codeexamples.analyzer.controllers.rest.commons.IndicatorType;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class CandleStickIndicator implements Indicator {
    @Override
    public boolean isApplicable(IndicatorType indicatorType, Map<String, Integer> parameters) {
        return IndicatorType.CANDLESTICK.equals(indicatorType);
    }

    @Override
    public List<Object[]> calculateData(List<Object[]> candles, LocalDateTime from, int groupingSize, Map<String, Integer> parameters) {
        List<Object[]> indicatorData = new ArrayList<>();
        if (groupingSize > 1) {
            Object[] groupedCandle = new Object[5];
            for (int i = 0; i < candles.size(); i++) {
                Object[] candle = candles.get(i);
                if (i%groupingSize == 0) {
                    groupedCandle = new Object[5];
                    groupedCandle[0] = candle[0];
                    groupedCandle[1] = candle[1];
                    groupedCandle[2] = candle[2];
                    groupedCandle[3] = candle[3];
                    groupedCandle[4] = candle[4];
                    continue;
                }

                // high
                if (((BigDecimal) groupedCandle[2]).compareTo((BigDecimal) candle[2]) < 0) {
                    groupedCandle[2] = candle[2];
                }

                // low
                if (((BigDecimal) groupedCandle[3]).compareTo((BigDecimal) candle[3]) > 0) {
                    groupedCandle[3] = candle[3];
                }

                if ((i+1)%groupingSize == 0) {
                    groupedCandle[4] = candle[4];
                    indicatorData.add(groupedCandle);
                }
            }
        } else {
            indicatorData = new ArrayList<>(candles);
        }

        indicatorData.removeIf(candle -> ((BigInteger) candle[0]).longValue() < from.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());

        return indicatorData;
    }

    @Override
    public String getName(Map<String, Integer> parameters) {
        return IndicatorType.CANDLESTICK.name();
    }

    @Override
    public int getZoomExtraCandlesCount(Map<String, Integer> parameters) {
        return 0;
    }
}

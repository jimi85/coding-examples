package com.codeexamples.analyzer.services.indicators;

import com.codeexamples.analyzer.controllers.rest.commons.IndicatorType;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

public interface Indicator {
    boolean isApplicable(IndicatorType indicatorType, Map<String, Integer> parameters);
    List<Object[]> calculateData(List<Object[]> candles, LocalDateTime from, int groupingSize, Map<String, Integer> parameters);
    String getName(Map<String, Integer> parameters);
    int getZoomExtraCandlesCount(Map<String, Integer> parameters);
}

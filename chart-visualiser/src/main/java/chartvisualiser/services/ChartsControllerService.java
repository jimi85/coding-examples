package com.codeexamples.analyzer.services;

import com.codeexamples.analyzer.controllers.rest.requests.ChartRequest;
import com.codeexamples.analyzer.controllers.rest.requests.SearchCandlesRequest;
import com.codeexamples.analyzer.controllers.rest.resources.ChartResource;
import com.codeexamples.analyzer.domain.dtos.CandleDto;
import com.codeexamples.analyzer.domain.dtos.CandleFilterDto;
import com.codeexamples.analyzer.domain.services.CandleService;
import com.codeexamples.analyzer.exceptions.MissingIndicatorException;
import com.codeexamples.analyzer.services.indicators.Indicator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.*;

@Service
@Slf4j
public class ChartsControllerService {

    private final int maxPoints;
    private final CandleService service;
    private final List<Indicator> indicators;

    public ChartsControllerService(@Value("${analyzer.graph.indicator.max-points}") int maxPoints, final CandleService service, final List<Indicator> indicators) {
        this.maxPoints = maxPoints;
        this.service = service;
        this.indicators = indicators;
    }

    public List<CandleDto> findCandles(SearchCandlesRequest request) {
        return service.findCandles(new CandleFilterDto(
                request.getSymbol(),
                request.getInterval().getMinutes(),
                request.getFrom(),
                request.getTo(),
                false,
                0
        ));
    }

    public ChartResource getChartIndicators(ChartRequest request) {
        log.info("start of charts");

        int zoomExtraCandlesCount = getZoomExtraCandlesCount(request);
        List<Object[]> candlesRaw = service.findCandlesRaw(new CandleFilterDto(
                request.getSymbol(),
                request.getInterval().getMinutes(),
                request.getFrom(),
                request.getTo(),
                request.isZoomed(),
                zoomExtraCandlesCount
        ));

        int groupingSize = Math.round((candlesRaw.size() - zoomExtraCandlesCount)/(float)maxPoints);

        List<ChartResource.IndicatorResource> indicatorResources = new ArrayList<>();
        request.getIndicators().parallelStream()
                .forEach(indicatorRequest -> indicatorResources.add(createIndicatorResource(indicatorRequest, candlesRaw, request.getFrom(), groupingSize)));
        log.info("end of charts");
        return new ChartResource(groupingSize > 1, indicatorResources);
    }

    private ChartResource.IndicatorResource createIndicatorResource(ChartRequest.IndicatorRequest request, List<Object[]> candles, LocalDateTime from, int groupingSize) {
        return new ChartResource.IndicatorResource(
                request.getId(),
                getIndicator(request).getName(request.getParameters()),
                request.getType(),
                getIndicator(request).calculateData(candles, from, groupingSize, request.getParameters())
        );
    }

    private int getZoomExtraCandlesCount(ChartRequest request) {
        int extraCandlesCount = 0;
        if (request.isZoomed()) {
            for (ChartRequest.IndicatorRequest indicatorRequest : Optional.ofNullable(request.getIndicators()).orElse(Collections.emptyList())) {
                Indicator indicator = getIndicator(indicatorRequest);
                extraCandlesCount = Math.max(extraCandlesCount, indicator.getZoomExtraCandlesCount(indicatorRequest.getParameters()));
            }
        }
        return extraCandlesCount;
    }

    private Indicator getIndicator(ChartRequest.IndicatorRequest request) {
        return indicators.stream()
                .filter(calculator -> calculator.isApplicable(request.getType(), request.getParameters()))
                .findFirst().orElseThrow(() -> new MissingIndicatorException(String.format("Missing indicator calculator for '%s'", request.getType())));
    }
}

package com.codeexamples.analyzer.services;

import com.codeexamples.analyzer.domain.services.imports.ImportService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ImportsControllerService {
    private final ImportService importService;

    public void importQuotesToDb() {
        importService.importQuotes();
        importService.aggregateCandles();
    }
}

package com.codeexamples.analyzer.domain.services.imports;

import com.codeexamples.analyzer.domain.entities.Currency;
import com.codeexamples.analyzer.domain.entities.Interval;
import com.codeexamples.analyzer.domain.repositories.ImportRepository;
import com.codeexamples.analyzer.domain.repositories.QuoteRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class ImportService {

    @Value("${analyzer.import.thread.pool-size:8}")
    private int threadPoolSize;

    private final QuoteRepository repository;
    private final ImportRepository importRepository;

    public void importQuotes() {
        ExecutorService executor = Executors.newFixedThreadPool(threadPoolSize);
        List<Callable<String>> threads = new ArrayList<>();
        try {
            List<String> files = Files.walk(Paths.get("./import-data"))
                    .filter(Files::isRegularFile)
                    .map(Path::getFileName)
                    .map(Path::toString)
                    .collect(Collectors.toList());

            files.forEach(file -> threads.add(new FileImportDbThread(file, repository)));
            executor.invokeAll(threads);
        } catch (IOException e) {
            log.error("Can't walk over paths", e);
        } catch (InterruptedException e) {
            log.error("Error when invoking threads", e);
        }
    }

    public void aggregateCandles() {
        ExecutorService executor = Executors.newFixedThreadPool(threadPoolSize);
        List<Callable<String>> threads = new ArrayList<>();
        try {
            for (Interval interval: Interval.values()) {
                threads.add(new AggregateCandlesDbThread(interval, "DE.30.", Currency.USD, importRepository));
            }

            executor.invokeAll(threads);
        } catch (InterruptedException e) {
            log.error("Error when invoking threads", e);
        }
    }
}

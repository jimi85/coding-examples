package com.codeexamples.analyzer.domain.services.imports;

import com.codeexamples.analyzer.domain.entities.Currency;
import com.codeexamples.analyzer.domain.entities.Interval;
import com.codeexamples.analyzer.domain.repositories.ImportRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.Callable;

@RequiredArgsConstructor
@Slf4j
public class AggregateCandlesDbThread implements Callable<String> {

    private final Interval interval;
    private final String symbol;
    private final Currency currency;
    private final ImportRepository repository;

    @Override
    public String call() {
        log.info(String.format("Start view creating '%s' with period '%s'", symbol, interval));
        repository.createCadlesMaterializedView(symbol, interval.getSeconds());
        log.info(String.format("End view creating '%s' with period '%s'", symbol, interval));
        log.info(String.format("Start aggregate '%s' with period '%s'", symbol, interval));
        repository.createCandlesFromQuotes(symbol, interval.getSeconds(), currency);
        log.info(String.format("End aggregate '%s' with period '%s'", symbol, interval));
        return "DONE";
    }
}

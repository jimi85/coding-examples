package com.codeexamples.analyzer.domain.services.imports;

import com.codeexamples.analyzer.domain.entities.Currency;
import com.codeexamples.analyzer.domain.entities.Quote;
import com.codeexamples.analyzer.domain.repositories.QuoteRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

@RequiredArgsConstructor
@Slf4j
public class FileImportDbThread implements Callable<String> {

    private final String fileName;
    private final QuoteRepository repository;

    @Override
    public String call() {
        try {
            String path = "./import-data/" + fileName;
            log.info(String.format("Importing: '%s'", path));
            List<Quote> quotes = new ArrayList<>();
            BufferedReader br = new BufferedReader(new FileReader(path));

            String line;
            int lineCounter = 0;
            int totalCount = 0;
            while ((line = br.readLine()) != null) {
                if (lineCounter == 0) {
                    lineCounter++;
                    continue;
                }
                String[] values = line.split(",");

                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                Quote quote = Quote.builder()
                        .symbol(values[0])
                        .created(LocalDateTime.parse(values[1] + " " + values[2], formatter))
                        .buy(BigDecimal.valueOf(Float.parseFloat(values[3])).setScale(3, RoundingMode.HALF_UP))
                        .sell(BigDecimal.valueOf(Float.parseFloat(values[4])).setScale(3, RoundingMode.HALF_UP))
                        .currency(Currency.USD)
                        .build();
                quotes.add(quote);
                lineCounter++;
                totalCount++;
                if (lineCounter%10000 == 0) {
                    repository.saveAll(quotes);
                    quotes.clear();
                }
            }
            repository.saveAll(quotes);
            log.info(String.format("Importing: '%s' DONE; Processed count %d", path, totalCount));
        } catch (FileNotFoundException e) {
            log.error("File not found", e);
        } catch (IOException e) {
            log.error("Read line error", e);
        }

        return "DONE";
    }
}

package com.codeexamples.analyzer.domain.services;

import com.codeexamples.analyzer.domain.dtos.CandleDto;
import com.codeexamples.analyzer.domain.dtos.CandleFilterDto;
import com.codeexamples.analyzer.domain.entities.Interval;
import com.codeexamples.analyzer.domain.repositories.CandleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CandleService {
    private final CandleRepository candleRepository;

    public List<CandleDto> findCandles(CandleFilterDto filterDto) {
        return candleRepository.findCandles(
                filterDto.getSymbol(),
                Interval.fromMinutes(filterDto.getInterval()),
                filterDto.getFrom(),
                filterDto.getTo()
        );
    }

    public List<Object[]> findCandlesRaw(CandleFilterDto filterDto) {
        return candleRepository.findCandlesRaw(
                filterDto.getSymbol(),
                Interval.fromMinutes(filterDto.getInterval()),
                filterDto.getFrom(),
                filterDto.getTo(),
                filterDto.isZoomed(),
                filterDto.getExtraCandlesCount()
        );
    }
}

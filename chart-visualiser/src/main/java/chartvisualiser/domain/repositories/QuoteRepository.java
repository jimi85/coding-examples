package com.codeexamples.analyzer.domain.repositories;

import com.codeexamples.analyzer.domain.entities.Quote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QuoteRepository extends JpaRepository<Quote, Long> {
}

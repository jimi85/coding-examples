package com.codeexamples.analyzer.domain.repositories;

import com.codeexamples.analyzer.domain.SymbolHelper;
import com.codeexamples.analyzer.domain.dtos.CandleDto;
import com.codeexamples.analyzer.domain.entities.Interval;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Repository
@Slf4j
public class CandleRepository {

    @PersistenceContext
    EntityManager entityManager;

    public List<Object[]> findCandlesRaw(String symbol, Interval interval, LocalDateTime from, LocalDateTime to, boolean zoomed, int extraCandlesCount) {
        String query = "";
        if (zoomed && extraCandlesCount > 0) {
            query += String.format("(SELECT * FROM (SELECT floor(unix_timestamp(open_time))*1000 as open_time, open, high, low, close FROM candles_%s_%s c WHERE c.open_time < :from ORDER BY c.open_time DESC LIMIT :extraCandlesCount) AS tmp ORDER BY open_time ASC) UNION ",
                    SymbolHelper.getSymbolForTable(symbol),
                    interval.getSeconds()
            );
        }

        query += String.format(
                "(SELECT floor(unix_timestamp(open_time))*1000 as open_time, open, high, low, close FROM candles_%s_%s c where c.open_time BETWEEN :from AND :to)",
                SymbolHelper.getSymbolForTable(symbol),
                interval.getSeconds()
        );

        Query q = entityManager.createNativeQuery(query);
        q.setParameter("from", from);
        q.setParameter("to", to);
        if (zoomed && extraCandlesCount > 0) {
            q.setParameter("extraCandlesCount", extraCandlesCount);
        }

        return (List<Object[]>) q.getResultList();
    }
}

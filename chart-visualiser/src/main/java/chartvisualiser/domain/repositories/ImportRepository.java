package com.codeexamples.analyzer.domain.repositories;

import com.codeexamples.analyzer.domain.SymbolHelper;
import com.codeexamples.analyzer.domain.entities.Currency;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

@Repository
@Slf4j
public class ImportRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    public void createCadlesMaterializedView(String symbol, long interval) {
        entityManager.createNativeQuery(
                String.format(
                        "CREATE TABLE IF NOT EXISTS candles_%s_%d (" +
                                "  id bigint(20) NOT NULL AUTO_INCREMENT," +
                                "  open_time datetime(6) NOT NULL," +
                                "  open decimal(19,3) NOT NULL," +
                                "  close decimal(19,3) NOT NULL," +
                                "  high decimal(19,3) NOT NULL," +
                                "  low decimal(19,3) NOT NULL," +
                                "  currency varchar(3) NOT NULL," +
                                "  PRIMARY KEY (id)," +
                                "  KEY open_time (open_time)" +
                                ") ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;",
                        SymbolHelper.getSymbolForTable(symbol),
                        interval
                ))
                .executeUpdate();
    }

    @Transactional
    public void createCandlesFromQuotes(String symbol, long interval, Currency currency) {
        String queryString = String.format(
                "INSERT INTO candles_%s_%d (open_time, open, close, high, low, currency) " +
                        "SELECT " +
                        "from_unixtime(unix_timestamp(created) - unix_timestamp(created) mod %d) as open_time," +
                        "(select buy from quotes as q2 where q2.created = min(q1.created)) as open_buy," +
                        "(select buy from quotes as q3 where q3.created = max(q1.created)) as close_buy," +
                        "max(q1.buy) as high_buy," +
                        "min(q1.buy) as low_buy," +
                        "'%s' " +
                        "FROM quotes as q1 " +
                        "WHERE symbol = '%s' GROUP BY open_time",
                SymbolHelper.getSymbolForTable(symbol),
                interval,
                interval,
                currency,
                symbol
        );
        Query query = entityManager.createNativeQuery(queryString);
        query.executeUpdate();
    }
}

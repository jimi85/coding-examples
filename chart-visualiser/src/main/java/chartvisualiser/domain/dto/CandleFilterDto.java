package com.codeexamples.analyzer.domain.dtos;

import lombok.Value;

import java.time.LocalDateTime;

@Value
public class CandleFilterDto {
    String symbol;
    long interval;
    LocalDateTime from;
    LocalDateTime to;
    boolean zoomed;
    int extraCandlesCount;
}

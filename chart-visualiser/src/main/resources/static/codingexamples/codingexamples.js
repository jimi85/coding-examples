
function getIndicatorParameters(indicator) {
    let inputs;
    switch (indicator) {
        case 'EMA':
        case 'SMA':
        case 'RSI':
            inputs = [getInputHtml("number", "period", "Period")];
            break;
        case 'MACD':
            inputs = [
                getInputHtml("number", "fma-period", "Fast MA period"),
                getInputHtml("number", "slma-period", "Slow MA period"),
                getInputHtml("number", "signal-period", "Signal period")
            ];
            break;
        case 'SAR':
            inputs = [
                getInputHtml("number", "min-af", "Minimum AF"),
                getInputHtml("number", "max-af", "Maximum AF")
            ];
            break;
        default:
            inputs = [];
    }

    return inputs.length === 0 ? "" : getParametersWithWrapingHtml("col-5 parameters", ...inputs);
}

function getIndicatorColors(indicator) {
    let inputs;
    let columnClass = "col-1";
    switch (indicator) {
        case 'CANDLESTICK':
            inputs = [
                getColorInputHtml("#00aa00", "increasing", "Choose color for increasing candle"),
                getColorInputHtml("#aa0000", "decreasing", "Choose color for decreasing candle")
            ];
            columnClass = "col-2";
            break;
        case 'EMA':
        case 'SMA':
        case 'RSI':
        case 'SAR':
            inputs = [
                getColorInputHtml(getRandomColorHex(), indicator, "Choose color for " + indicator)
            ];
            break;
        case 'MACD':
            inputs = [
                getColorInputHtml(getRandomColorHex(), indicator, "Choose color for " + indicator),
                getColorInputHtml(getRandomColorHex(), "signal", "Choose color for MACD signal"),
                getColorInputHtml(getRandomColorHex(), "increasing", "Choose color for increasing bar"),
                getColorInputHtml(getRandomColorHex(), "decreasing", "Choose color for decreasing bar")
            ];
            columnClass = "col-3";
            break;
        default:
            inputs = [];
    }

    return inputs.length === 0 ? "" : getParametersWithWrapingHtml(columnClass + " colors", ...inputs);
}

function completeIndicators(indicator) {
    return getIndicatorParameters(indicator) + getIndicatorColors(indicator);
}

function getParametersWithWrapingHtml(wrapperClass, ...innerHtml) {
    let html = '<div class="' + wrapperClass + '">';
    html += '<div class="row">';
    html += innerHtml.join('');
    html += '</div></div>';
    return html
}

function getColorInputHtml(color, name, description) {
    let colorInputHtml = '';
    colorInputHtml += '<input type="color" class="form-control form-control-color me-1" ' +
        ' name="indicators[][colors]['+name+']" value="' + color + '" title="' + description + '">';
    return colorInputHtml;
}

function getInputHtml(type, name, description) {
    let inputHtml = '<div class="col">';
    inputHtml += '<input type="' + type + '" name="indicators[][parameters]['+name+']" autocomplete="off"' +
        ' class="form-control indicator-parameter" placeholder="'+description+'" step="any"/>';
    inputHtml += "</div>";
    return inputHtml;
}

function getIndicatorHtml() {
    let html = '<div class="row indicator-row">';
    html += '<div class="col-3">';
    html += '<select name="indicators[][type]" class="indicator form-select">';
    html += '<option value="">Select indicator</option>';
    html += '<option value="CANDLESTICK">Candle-stick</option>';
    html += '<option value="EMA">MA - exponencial</option>';
    html += '<option value="SMA">MA - simple</option>';
    html += '<option value="MACD">MACD</option>';
    html += '<option value="RSI">RSI</option>';
    html += '<option value="SAR">SAR</option>';
    html += '</select>';
    html += '</div>';
    html += '<div class="col align-middle lh-lg">';
    html += '<i class="add-indicator bi bi-plus-circle-fill text-success fs-5" role="button"></i>';
    html += '<i class="remove-indicator bi bi-trash-fill text-danger fs-5" role="button"></i>';
    html += '</div>';
    html += '</div>';
    return html;
}

function getRandomColorHex() {
    const randomColor = Math.floor(Math.random() * 16777215).toString(16);
    return "#" + randomColor;
}

function isStringNumeric(str) {
    if (typeof str != "string") return false;
    return !isNaN(str) && !isNaN(parseFloat(str));
}

function isValid(indicator) {
    if (!indicator.type || (!!indicator.type && indicator.type.trim() === "")) {
        return false;
    }

    switch (indicator.type) {
        case 'EMA':
        case 'SMA':
        case 'RSI':
            if (!indicator.parameters || (!!indicator.parameters && !indicator.parameters.period)) {
                return false;
            }
            if (!isStringNumeric(indicator.parameters.period)) {
                return false;
            }
            break;
        case 'MACD':
            if (!indicator.parameters
                || (
                    !!indicator.parameters && (
                        !indicator.parameters['fma-period']
                        || !indicator.parameters['slma-period']
                        || !indicator.parameters['signal-period']
                    ))
            ) {
                return false;
            }
            if (!isStringNumeric(indicator.parameters['fma-period'])
                || !isStringNumeric(indicator.parameters['slma-period'])
                || !isStringNumeric(indicator.parameters['signal-period'])
            ) {
                return false;
            }
            break;
        case 'SAR':
            if (!indicator.parameters
                || (!!indicator.parameters && (!indicator.parameters['min-af'] || !indicator.parameters['max-af'] ))
            ) {
                return false;
            }
            if (!isStringNumeric(indicator.parameters['min-af']) || !isStringNumeric(indicator.parameters['max-af'])) {
                return false;
            }
            break;
    }

    return true;
}

function getIndicatorColor(indicatorId, indicators, colorName) {
    let indicatorParams = indicatorId.split("-");
    indicators = indicators.filter(indicator => indicator.type === indicatorParams[0]);

    switch (indicatorParams[0]) {
        case 'CANDLESTICK':
            return indicators[0].colors[colorName];
        case 'EMA':
        case 'SMA':
        case 'RSI':
            return indicators.filter(indicator => indicator.parameters.period === parseInt(indicatorParams[1]))[0]
                .colors[colorName];
        case 'MACD':
            return "";
            // this.parameters['fma-period'] = parseInt(this.parameters['fma-period']);
            // this.parameters['slma-period'] = parseInt(this.parameters['slma-period']);
            // this.parameters['signal-period'] = parseInt(this.parameters['signal-period']);
            // break;
        case 'SAR':
            return "";
            // this.parameters['min-af'] = parseInt(this.parameters['min-af']);
            // this.parameters['max-af'] = parseInt(this.parameters['max-af']);
            // break;
    }
}
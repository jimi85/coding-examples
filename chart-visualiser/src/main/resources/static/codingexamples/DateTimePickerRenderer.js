class DateTimePickerRenderer {
    #datetimePickerOptions = {
        format: 'DD.MM.YYYY HH:mm',
        currentDate: moment("2014-01-01"),
        clearButton: true,
        nowButton: true,
        switchOnClick: true,
        weekStart: 1,
        lang: 'cs'
    };

    #timePickerOptions = {
        format: 'HH:mm',
        clearButton: true,
        nowButton: true,
        switchOnClick: true,
        year: false,
        date: false,
        lang: 'cs'
    };

    #datetimePickerSelector;
    #timePickerSelector;

    constructor(dateTimePickerSelector, timePickerSelector) {
        this.#datetimePickerSelector = dateTimePickerSelector;
        this.#timePickerSelector = timePickerSelector;
    }

    renderDateTimePicker() {
        $(this.#datetimePickerSelector).bootstrapMaterialDatePicker(this.#datetimePickerOptions);
    }

    renderTimePicker() {
        $(this.#timePickerSelector).bootstrapMaterialDatePicker(this.#timePickerOptions);
    }

    renderAll() {
        this.renderDateTimePicker();
        this.renderTimePicker();
    }
}
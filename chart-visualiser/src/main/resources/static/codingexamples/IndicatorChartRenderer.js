class IndicatorChartRenderer {
    #chartOptions = {
        chart: {
            zoomType: ['x']
        },
        time: {
            timezone: moment.tz.guess()
        },
        legend: {
            enabled: true
        },
        title: {
            text: 'Stock graph'
        },
        subtitle: {
            text: '',
            style: {color: "#198754", fontWeight: 'bold'}
        },
        scrollbar: {
            liveRedraw: false
        },
        navigator: {
            adaptToUpdatedData: false,
            series: [{
                name: 'Chart navigator',
                data: []
            }]
        },
        rangeSelector: {
            buttons: [
                {
                    type: 'hour',
                    count: 1,
                    text: '1H'
                }, {
                    type: 'day',
                    count: 1,
                    text: '1D'
                }, {
                    type: 'month',
                    count: 1,
                    text: '1M'
                }, {
                    type: 'year',
                    count: 1,
                    text: '1Y'
                }, {
                    type: 'all',
                    text: 'All'
                }],
            inputEnabled: false, // it supports only days
            selected: 4 // all
        },
        xAxis: {
            events: {
                afterSetExtremes: this.searchIndicatorsZoomed
            },
            minRange: 3600 * 1000 // 1 hour
        },
        plotOptions: {
            series: {
                dataGrouping: {
                    enabled: false
                }
            }
        },
        series: [],
        lang: {
            noData: "Žádná data"
        },
        noData: {
            style: {
                fontWeight: 'bold',
                fontSize: '15px',
                color: '#303030'
            }
        }
    };

    #chartDivId;
    #chart;
    static #parametersData;
    static #indicatorsData;

    constructor(chartDivId) {
        this.#chartDivId = chartDivId;
    }

    renderChart() {
        this.#chart = Highcharts.stockChart(this.#chartDivId, this.#chartOptions);
    }

    searchIndicators(parametersData, indicatorsData) {
        IndicatorChartRenderer.#parametersData = parametersData;
        IndicatorChartRenderer.#indicatorsData = indicatorsData;
        IndicatorChartRenderer.#indicatorsData.indicators = this.convertFormIndicatorsToRequestIndicators(indicatorsData.indicators);

        if (!!IndicatorChartRenderer.#parametersData.from && !!IndicatorChartRenderer.#parametersData.to && !!IndicatorChartRenderer.#parametersData.symbol && !!IndicatorChartRenderer.#parametersData.interval
            && !!IndicatorChartRenderer.#indicatorsData && !!IndicatorChartRenderer.#indicatorsData.indicators && IndicatorChartRenderer.#indicatorsData.indicators.length > 0
        ) {
            let searchData = IndicatorChartRenderer.#parametersData;
            searchData.zoomed = false;
            searchData.from = moment(searchData.from.replace(" ", "T"), "DD.MM.YYYYTHH:mm").format("YYYY-MM-DDTHH:mm");
            searchData.to = moment(searchData.to.replace(" ", "T"), "DD.MM.YYYYTHH:mm").format("YYYY-MM-DDTHH:mm");
            searchData.indicators = [];
            $.each(IndicatorChartRenderer.#indicatorsData.indicators, function () {
                searchData.indicators.push(
                    {
                        id: IndicatorChartRenderer.getIndicatorId(this),
                        type: this.type,
                        parameters: this.parameters,
                    }
                );
            });

            this.#chart.showLoading("Nahrávám");
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=UTF-8",
                dataType: "json",
                data: JSON.stringify(searchData),
                url: "/charts/search-indicators",
                context: this,
                success: function (response) {
                    this.#chartOptions.series = [];
                    this.#chartOptions.title.text = searchData.symbol;

                    if (response.zoomable) {
                        this.#chartOptions.subtitle.text = 'ZOOMABLE';
                    }

                    console.time('addSeries');
                    $.each.call(this, response.indicators, (index, indicator) => {
                        if (this.#chartOptions.series.filter(serie => serie.id === indicator.id).length > 0) return;

                        // if (index === 0) {
                        //     this.#chartOptions.navigator.series[0].data.push(indicator.data);
                        //     this.#chartOptions.navigator.series[0].type = "line";
                        // }

                        switch (indicator.type) {
                            case "CANDLESTICK":
                                this.#chartOptions.series.push(
                                    {
                                        id: indicator.id,
                                        type: 'candlestick',
                                        name: indicator.name,
                                        color: getIndicatorColor(indicator.id, indicatorsData.indicators, 'decreasing'),
                                        upColor: getIndicatorColor(indicator.id, indicatorsData.indicators, 'increasing'),
                                        data: indicator.data
                                    }
                                );
                                break;
                            case "SMA":
                                this.#chartOptions.series.push(
                                    {
                                        id: indicator.id,
                                        type: 'line',
                                        name: indicator.name,
                                        color: getIndicatorColor(indicator.id, indicatorsData.indicators, indicator.type),
                                        data: indicator.data
                                    }
                                );
                                break;
                        }
                    });

                    this.#chart = Highcharts.stockChart(this.#chartDivId, this.#chartOptions);
                    console.timeEnd('addSeries');
                    this.#chart.hideLoading();
                },
                error: function (data) {
                    console.log(data)
                }
            });
        }
    }

    searchIndicatorsZoomed(e) {
        if (!e.max || !e.min) {
            return;
        }
        let {chart} = e.target;
        let searchData = IndicatorChartRenderer.#parametersData;
        searchData.from = moment(Math.round(e.min)).format("YYYY-MM-DDTHH:mm");
        searchData.to = moment(Math.round(e.max)).format("YYYY-MM-DDTHH:mm");
        searchData.zoomed = true;
        searchData.indicators = [];

        $.each(IndicatorChartRenderer.#indicatorsData.indicators, function () {
            searchData.indicators.push(
                {
                    id: IndicatorChartRenderer.getIndicatorId(this),
                    type: this.type,
                    parameters: this.parameters,
                }
            );
        });

        chart.showLoading("Nahrávám");
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=UTF-8",
            dataType: "json",
            data: JSON.stringify(searchData),
            url: "/charts/search-indicators",
            success: function (response) {
                console.time('addSeries');
                if (response.zoomable === false || response.zoomable === undefined || response.zoomable === null) {
                    chart.setTitle(null, {text: ''});
                } else {
                    chart.setTitle(null, {text: 'ZOOMABLE'});
                }
                $.each(response.indicators, function () {
                    if (!!chart.get(this.id)) {
                        chart.get(this.id).setData(this.data, false);
                    }
                });
                console.timeEnd('addSeries');
                chart.redraw();
                chart.hideLoading();
            },
            error: function (data) {
                console.log(data)
            }
        });
    }

    removeIndicator(indicator) {
        const removedSerie = this.#chart.get(IndicatorChartRenderer.getIndicatorId(indicator));
        if (!!removedSerie) {
            removedSerie.remove();
        }
    }

    static getIndicatorId(indicator) {
        switch (indicator.type) {
            case 'CANDLESTICK':
                return indicator.type;
            case 'EMA':
            case 'SMA':
            case 'RSI':
                return indicator.type + "-" + indicator.parameters.period;
            case 'MACD':
                return indicator.type + "-" + indicator.parameters['fma-period'] + "-" + indicator.parameters['slma-period'] + "-" + indicator.parameters['signal-period'];
            case 'SAR':
                return indicator.type + "-" + indicator.parameters['min-af'] + "-" + indicator.parameters['max-af'];
            default:
                return "";
        }
    }

    convertFormIndicatorsToRequestIndicators(indicators) {
        indicators = indicators.filter(indicator => isValid(indicator));

        $.each(indicators, function () {
            switch (this.type) {
                case 'EMA':
                case 'SMA':
                case 'RSI':
                    this.parameters.period = parseInt(this.parameters.period);
                    break;
                case 'MACD':
                    this.parameters['fma-period'] = parseInt(this.parameters['fma-period']);
                    this.parameters['slma-period'] = parseInt(this.parameters['slma-period']);
                    this.parameters['signal-period'] = parseInt(this.parameters['signal-period']);
                    break;
                case 'SAR':
                    this.parameters['min-af'] = parseInt(this.parameters['min-af']);
                    this.parameters['max-af'] = parseInt(this.parameters['max-af']);
                    break;
            }
        });

        return indicators;
    }
}